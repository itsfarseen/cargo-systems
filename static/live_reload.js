var ws = new WebSocket("ws://" + window.location.hostname + ":8001");
ws.onmessage = function (evt) {
  if (evt.data == "reload") {
    console.log("Reload", evt);
    window.setTimeout(function () {
      document.location.reload();
    }, 500);
  }
  if (evt.data == "reload_delayed") {
    console.log("Reload Delayed", evt);
    window.setTimeout(function () {
      document.location.reload();
    }, 8000);
  }
};

ws.onclose = function (evt) {
  console.log("Close", evt);
  window.setTimeout(function () {
    document.location.reload();
  }, 5000);
};
