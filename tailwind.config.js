module.exports = {
  purge: ["./templates/*.tera", "./templates/**/*.tera"],
  theme: {
    extend: {
      flex: {
        '2':'2 2 0%', 
      },
      screens: {
        'print': {'raw': 'print'}, // generates @media print {..}
      }
    },
  },
  variants: {
    backgroundColor: ['responsive', 'hover', 'active', 'focus'],
  },
  plugins: [],
};
