src_files       := $(shell find crates/ -type f)
debug_target    := target/debug/main
release_target  := target/release/main
example_configs	:= $(wildcard *.example) .env.example
migrations      := $(shell find migrations/ -type f)
templates       := $(shell find templates/ -type f)
static          := $(shell find static/ -type f)
static_exclude  := static/css/% static/logo.jpeg static/live_reload.js
static          := $(filter-out $(static_exclude), $(static))
pcss            := $(wildcard static/css/*.pcss)
debug_css       := $(patsubst %.pcss, %.css, $(pcss))

dist_dir              := dist
dist_exe              := $(dist_dir)/main
dist_css			  := $(patsubst %.pcss, dist/%.css, $(pcss))

dist_migrations       := $(addprefix $(dist_dir)/, $(migrations))
dist_example_configs  := $(addprefix $(dist_dir)/, $(example_configs))
dist_templates        := $(addprefix $(dist_dir)/, $(templates))
dist_static           := $(addprefix $(dist_dir)/, $(static))

dist_files_to_copy    := $(dist_migrations) $(dist_example_configs) $(dist_templates) $(dist_static)

.PHONY: debug_run

debug_run: $(debug_target)
	$(debug_target)

test_server: $(debug_target)
	ENV_TEST=true $(debug_target)

debug_css: $(debug_css)

release_run: $(release_target)
	$(release_target)

$(debug_css): %.css : %.pcss
	yarn install
	yarn postcss $< -o $@

$(release_target): $(src_files)
	cargo build --release

$(debug_target): $(src_files)
	cargo build

dist-all: $(dist_exe) $(dist_files_to_copy) $(dist_css)

dist-clean:
	rm -rf $(dist_dir)/*

$(dist_exe): $(release_target)
	@mkdir -p $(dist_dir)
	cp $(release_target) $(dist_dir)/main

$(dist_css): $(dist_dir)/%.css : %.pcss
	yarn install
	NODE_ENV=production yarn postcss $< -o $@

$(dist_files_to_copy): $(dist_dir)/% : %
	@mkdir -p $(@D)
	cp $< $@

docker-build:
	docker build . -t cargo-ubuntu

docker-run: docker-build
	docker run -it \
		--volume=`pwd`/docker/dist:/project/dist \
		--volume=`pwd`/docker/target:/project/target \
		--volume=`pwd`/docker/node_modules:/project/node_modules \
		--volume=`pwd`/docker/cargo-registry:/root/.cargo/registry \
		cargo-ubuntu

docker-shell: docker-build
	docker run -it \
		--volume=`pwd`/docker/dist:/project/dist \
		--volume=`pwd`/docker/target:/project/target \
		--volume=`pwd`/docker/node_modules:/project/node_modules \
		--volume=`pwd`/docker/cargo-registry:/root/.cargo/registry \
		cargo-ubuntu /bin/bash