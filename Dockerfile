FROM ubuntu:18.04

RUN apt-get update
RUN apt-get install -y curl

RUN curl https://sh.rustup.rs -sSf | sh -s -- --profile minimal --default-toolchain nightly-2020-12-06 -y -v
ENV PATH=/root/.cargo/bin:${PATH}

RUN apt-get install -y libpq-dev libssl-dev make gcc pkg-config

RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install -y nodejs

RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update
RUN apt-get install -y yarn

WORKDIR /project
COPY . .

CMD make dist-clean dist-all