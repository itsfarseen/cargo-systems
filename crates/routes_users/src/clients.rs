use rocket::{request::Form, response::Redirect, response::Responder, Route};
use std::str::FromStr;

use crate::form::FormValue;
use database::user::password_utils::password_hash;
use database::user::*;
use database::IdType;
use view_base::app_page::AppPage;
use view_base::session::AdminSession;
use db_provider::Db;

pub fn routes() -> Vec<Route> {
    routes![
        clients,
        clients_item,
        clients_register,
        clients_register_submit,
        clients_edit,
        clients_edit_submit,
    ]
}

#[derive(Serialize)]
struct ClientsPage {
    clients_with_info: Vec<(User, Option<ClientInfo>)>,
}

#[get("/clients")]
fn clients(_session: AdminSession, db: Db) -> AppPage<ClientsPage> {
    let clients = {
        let mut admins = User::by_role(UserRole::Admin, &db.0);
        let mut clients: Vec<User> = User::by_role(UserRole::Client, &db.0);
        admins.append(&mut clients);
        admins
    };

    let client_infos = ClientInfo::by_users(&clients, &db.0);

    let clients_with_info: Vec<(User, Option<ClientInfo>)> =
        clients.into_iter().zip(client_infos.into_iter()).collect();

    let page = ClientsPage { clients_with_info };
    AppPage::with_template("clients/index", page)
}

#[get("/clients/<id>")]
fn clients_item(_session: AdminSession, db: Db, id: IdType) -> impl Responder<'static> {
    let client = User::by_id(id, &db.0);
    match client {
        Some(client) => {
            let client_info = ClientInfo::by_id(id, &db.0);
            Some(AppPage::with_template(
                "clients/item",
                (client, client_info),
            ))
        }
        None => None,
    }
}

#[derive(Serialize, Default)]
struct RegisterPage {
    name: FormValue,
    username: FormValue,
    password: FormValue,
    password_confirm: FormValue,
    phone: FormValue,
    address: FormValue,
    delivery_charge: FormValue,
    return_charge: FormValue,
}

#[get("/clients/register")]
fn clients_register(_session: AdminSession) -> impl Responder<'static> {
    let page = RegisterPage::default();
    AppPage::with_template("clients/register", page)
}
#[derive(FromForm)]
struct ClientsRegisterForm {
    name: String,
    username: String,
    password: String,
    password_confirm: String,
    phone: String,
    address: String,
    delivery_charge: String,
    return_charge: String,
}

#[post("/clients/register", data = "<form>")]
fn clients_register_submit(
    _session: AdminSession,
    form: Form<ClientsRegisterForm>,
    db: Db,
) -> impl Responder<'static> {
    let mut page = RegisterPage::default();
    let mut validation_ok = true;

    let name = form.name.trim();
    page.name.value = Some(name.to_owned());
    if name.is_empty() {
        validation_ok = false;
        page.name.error = Some("Name is empty".to_owned());
    } else {
        page.name.success = Some("Name is okay".to_owned());
    }

    let username = form.username.trim().to_lowercase();
    if username.is_empty() {
        validation_ok = false;
        page.username.error = Some("Username is empty".to_owned());
    } else if username.len() < 6 {
        validation_ok = false;
        page.username.error = Some("Username must be larger than 6 letters.".to_owned());
    } else if User::by_username(&username, &db.0).is_some() {
        validation_ok = false;
        page.username.error = Some("Username already taken.".to_owned());
    } else {
        page.username.success = Some("Username is okay".to_owned());
    }
    page.username.value = Some(username);

    let password = form.password.trim().to_owned();
    if password.is_empty() {
        validation_ok = false;
        page.password.error = Some("Password is empty".to_owned());
    } else if password.len() < 6 {
        validation_ok = false;
        page.password.error = Some("Password must be larger than 6 letters.".to_owned());
    } else {
        page.password.success = Some("Password is okay".to_owned());
    }

    let password_confirm = form.password_confirm.trim().to_owned();
    if !password_confirm.eq(&password) {
        validation_ok = false;
        page.password_confirm.error = Some("Password confirmation doesn't match".to_owned());
    } else {
        page.password_confirm.success = Some("Password confirmation is okay".to_owned());
    }

    page.password.value = Some(password);
    page.password_confirm.value = Some(password_confirm);

    let phone = form.phone.trim();
    page.phone.value = Some(phone.to_owned());
    if phone.is_empty() {
        validation_ok = false;
        page.phone.error = Some("Phone is empty".to_owned());
    } else {
        page.phone.success = Some("Phone is okay".to_owned());
    }

    let address = form.address.trim();
    page.address.value = Some(address.to_owned());
    if address.is_empty() {
        validation_ok = false;
        page.address.error = Some("Address is empty".to_owned());
    } else {
        page.address.success = Some("Address is okay".to_owned());
    }

    let delivery_charge = form.delivery_charge.trim().to_owned();
    let delivery_charge_num: f64 = match f64::from_str(&delivery_charge) {
        Err(_) => {
            validation_ok = false;
            page.delivery_charge.error = Some("Delivery charge must be a whole number".to_owned());
            0f64
        }
        Ok(x) if x < 0f64 => {
            validation_ok = false;
            page.delivery_charge.error =
                Some("Delivery charge must be a positive number".to_owned());
            0f64
        }
        Ok(x) => {
            page.delivery_charge.success = Some("Delivery charge is okay".to_owned());
            x
        }
    };
    page.delivery_charge.value = Some(delivery_charge);

    let return_charge = form.return_charge.trim().to_owned();
    let return_charge_num = match f64::from_str(&return_charge) {
        Err(_) => {
            validation_ok = false;
            page.return_charge.error = Some("Return charge must be a whole number".to_owned());
            0f64
        }
        Ok(x) if x < 0f64 => {
            validation_ok = false;
            page.return_charge.error = Some("Return charge must be a positive number".to_owned());
            0f64
        }
        Ok(x) => {
            page.return_charge.success = Some("Return charge is okay".to_owned());
            x
        }
    };
    page.return_charge.value = Some(return_charge);

    let id = if validation_ok {
        let new_client = NewUser {
            name: name.to_owned(),
            username: page.username.value.clone().unwrap(),
            hashword: password_hash(page.password.value.as_ref().unwrap()),
            phone: phone.to_owned(),
            address: address.to_owned(),
            user_role: UserRole::Client,
        };
        let id = new_client.save(&db.0);
        match id {
            Some(id) => {
                ClientInfo {
                    client_id: id,
                    delivery_charge: delivery_charge_num,
                    return_charge: return_charge_num,
                }
                .save(&db.0);
                Some(id)
            }
            None => {
                page.username.error = Some("Username already taken".to_owned());
                page.username.success = None;
                None
            }
        }
    } else {
        None
    };
    if id.is_some() {
        Err(Redirect::to(uri!(clients)))
    } else {
        Ok(AppPage::with_template("clients/register", page))
    }
}
#[get("/clients/<id>?edit")]
fn clients_edit(_session: AdminSession, id: IdType, db: Db) -> impl Responder<'static> {
    let client = User::by_id(id, &db)?;
    let client_info = ClientInfo::by_id(id, &db)?;
    let mut page = RegisterPage::default();
    page.name.value = Some(client.name);
    page.username.value = Some(client.username);
    page.password.value = None;
    page.password_confirm.value = None;
    page.phone.value = Some(client.phone);
    page.address.value = Some(client.address);
    page.delivery_charge.value = Some(client_info.delivery_charge.to_string());
    page.return_charge.value = Some(client_info.return_charge.to_string());

    Some(AppPage::with_template("clients/register", page))
}

#[post("/clients/<id>?edit", data = "<form>")]
fn clients_edit_submit(
    _session: AdminSession,
    id: IdType,
    form: Form<ClientsRegisterForm>,
    db: Db,
) -> impl Responder<'static> {
    let mut page = RegisterPage::default();
    let mut validation_ok = true;

    let name = form.name.trim();
    page.name.value = Some(name.to_owned());
    if name.is_empty() {
        validation_ok = false;
        page.name.error = Some("Name is empty".to_owned());
    } else {
        page.name.success = Some("Name is okay".to_owned());
    }

    let username = form.username.trim().to_lowercase();
    if username.is_empty() {
        validation_ok = false;
        page.username.error = Some("Username is empty".to_owned());
    } else if username.len() < 6 {
        validation_ok = false;
        page.username.error = Some("Username must be larger than 6 letters.".to_owned());
    } else if User::by_username(&username, &db.0).is_some() {
        validation_ok = false;
        page.username.error = Some("Username already taken.".to_owned());
    } else {
        page.username.success = Some("Username is okay".to_owned());
    }
    page.username.value = Some(username);

    let password = form.password.trim().to_owned();
    if password.is_empty() {
        validation_ok = false;
        page.password.error = Some("Password is empty".to_owned());
    } else if password.len() < 6 {
        validation_ok = false;
        page.password.error = Some("Password must be larger than 6 letters.".to_owned());
    } else {
        page.password.success = Some("Password is okay".to_owned());
    }

    let password_confirm = form.password_confirm.trim().to_owned();
    if !password_confirm.eq(&password) {
        validation_ok = false;
        page.password_confirm.error = Some("Password confirmation doesn't match".to_owned());
    } else {
        page.password_confirm.success = Some("Password confirmation is okay".to_owned());
    }

    page.password.value = Some(password);
    page.password_confirm.value = Some(password_confirm);

    let phone = form.phone.trim();
    page.phone.value = Some(phone.to_owned());
    if phone.is_empty() {
        validation_ok = false;
        page.phone.error = Some("Phone is empty".to_owned());
    } else {
        page.phone.success = Some("Phone is okay".to_owned());
    }

    let address = form.address.trim();
    page.address.value = Some(address.to_owned());
    if address.is_empty() {
        validation_ok = false;
        page.address.error = Some("Address is empty".to_owned());
    } else {
        page.address.success = Some("Address is okay".to_owned());
    }

    let delivery_charge = form.delivery_charge.trim().to_owned();
    let delivery_charge_num: f64 = match f64::from_str(&delivery_charge) {
        Err(_) => {
            validation_ok = false;
            page.delivery_charge.error = Some("Delivery charge must be a whole number".to_owned());
            0f64
        }
        Ok(x) if x < 0f64 => {
            validation_ok = false;
            page.delivery_charge.error =
                Some("Delivery charge must be a positive number".to_owned());
            0f64
        }
        Ok(x) => {
            page.delivery_charge.success = Some("Delivery charge is okay".to_owned());
            x
        }
    };
    page.delivery_charge.value = Some(delivery_charge);

    let return_charge = form.return_charge.trim().to_owned();
    let return_charge_num = match f64::from_str(&return_charge) {
        Err(_) => {
            validation_ok = false;
            page.return_charge.error = Some("Return charge must be a whole number".to_owned());
            0f64
        }
        Ok(x) if x < 0f64 => {
            validation_ok = false;
            page.return_charge.error = Some("Return charge must be a positive number".to_owned());
            0f64
        }
        Ok(x) => {
            page.return_charge.success = Some("Return charge is okay".to_owned());
            x
        }
    };
    page.return_charge.value = Some(return_charge);

    let id = if validation_ok {
        let client = User {
            id,
            name: name.to_owned(),
            username: page.username.value.clone().unwrap(),
            hashword: password_hash(page.password.value.as_ref().unwrap()),
            phone: phone.to_owned(),
            address: address.to_owned(),
            user_role: UserRole::Client,
        };
        client.update(&db.0);
        ClientInfo {
            client_id: id,
            delivery_charge: delivery_charge_num,
            return_charge: return_charge_num,
        }
        .save(&db.0);
        Some(id)
    } else {
        None
    };
    if id.is_some() {
        Err(Redirect::to(uri!(clients_item: id.unwrap())))
    } else {
        Ok(AppPage::with_template("clients/register", page))
    }
}
