#![feature(decl_macro)]

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate serde_derive;

use rocket::Route;

mod clients;
mod drivers;
mod form;

pub fn routes() -> Vec<Route> {
    let mut routes = Vec::new();
    routes.append(&mut clients::routes());
    routes.append(&mut drivers::routes());
    routes
}
