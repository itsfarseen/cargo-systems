use crate::form::FormValue;
use database::user::password_utils::password_hash;
use database::user::{NewUser, User, UserRole};
use database::IdType;
use rocket::request::Form;
use rocket::response::{Redirect, Responder};
use rocket::Route;
use serde_derive::Serialize;
use view_base::app_page::AppPage;
use view_base::session::AdminSession;
use db_provider::Db;

pub fn routes() -> Vec<Route> {
    routes![
        drivers,
        drivers_item,
        drivers_register,
        drivers_register_submit,
    ]
}

#[derive(Serialize)]
struct DriversPage {
    drivers: Vec<User>,
}

#[get("/drivers")]
fn drivers(_session: AdminSession, db: Db) -> AppPage<DriversPage> {
    let drivers = User::by_role(UserRole::Driver, &db.0);
    let page = DriversPage { drivers };
    AppPage::with_template("drivers/index", page)
}

#[get("/drivers/<id>")]
fn drivers_item(_session: AdminSession, db: Db, id: IdType) -> impl Responder<'static> {
    let driver = User::by_id(id, &db.0);
    match driver {
        Some(driver) if driver.user_role == UserRole::Driver => {
            Some(AppPage::with_template("drivers/item", driver))
        }
        _ => None,
    }
}

#[derive(Serialize, Default)]
struct RegisterPage {
    name: FormValue,
    username: FormValue,
    password: FormValue,
    password_confirm: FormValue,
    phone: FormValue,
    address: FormValue,
    delivery_charge: FormValue,
    return_charge: FormValue,
}

#[get("/drivers/register")]
fn drivers_register(_session: AdminSession) -> impl Responder<'static> {
    let page = RegisterPage::default();
    AppPage::with_template("drivers/register", page)
}
#[derive(FromForm)]
struct DriversRegisterForm {
    name: String,
    username: String,
    password: String,
    password_confirm: String,
    phone: String,
    address: String,
}

#[post("/drivers/register", data = "<form>")]
fn drivers_register_submit(
    _session: AdminSession,
    form: Form<DriversRegisterForm>,
    db: Db,
) -> impl Responder<'static> {
    let mut page = RegisterPage::default();
    let mut validation_ok = true;

    let name = form.name.trim();
    page.name.value = Some(name.to_owned());
    if name.is_empty() {
        validation_ok = false;
        page.name.error = Some("Name is empty".to_owned());
    } else {
        page.name.success = Some("Name is okay".to_owned());
    }

    let username = form.username.trim().to_lowercase();
    if username.is_empty() {
        validation_ok = false;
        page.username.error = Some("Username is empty".to_owned());
    } else if username.len() < 6 {
        validation_ok = false;
        page.username.error = Some("Username must be larger than 6 letters.".to_owned());
    } else if User::by_username(&username, &db.0).is_some() {
        validation_ok = false;
        page.username.error = Some("Username already taken.".to_owned());
    } else {
        page.username.success = Some("Username is okay".to_owned());
    }
    page.username.value = Some(username);

    let password = form.password.trim().to_owned();
    if password.is_empty() {
        validation_ok = false;
        page.password.error = Some("Password is empty".to_owned());
    } else if password.len() < 6 {
        validation_ok = false;
        page.password.error = Some("Password must be larger than 6 letters.".to_owned());
    } else {
        page.password.success = Some("Password is okay".to_owned());
    }

    let password_confirm = form.password_confirm.trim().to_owned();
    if !password_confirm.eq(&password) {
        validation_ok = false;
        page.password_confirm.error = Some("Password confirmation doesn't match".to_owned());
    } else {
        page.password_confirm.success = Some("Password confirmation is okay".to_owned());
    }

    page.password.value = Some(password);
    page.password_confirm.value = Some(password_confirm);

    let phone = form.phone.trim();
    page.phone.value = Some(phone.to_owned());
    if phone.is_empty() {
        validation_ok = false;
        page.phone.error = Some("Phone is empty".to_owned());
    } else {
        page.phone.success = Some("Phone is okay".to_owned());
    }

    let address = form.address.trim();
    page.address.value = Some(address.to_owned());
    if address.is_empty() {
        validation_ok = false;
        page.address.error = Some("Address is empty".to_owned());
    } else {
        page.address.success = Some("Address is okay".to_owned());
    }

    let id = if validation_ok {
        let new_driver = NewUser {
            name: name.to_owned(),
            username: page.username.value.clone().unwrap(),
            hashword: password_hash(page.password.value.as_ref().unwrap()),
            phone: phone.to_owned(),
            address: address.to_owned(),
            user_role: UserRole::Driver,
        };
        let id = new_driver.save(&db.0);
        match id {
            Some(id) => Some(id),
            None => {
                page.username.error = Some("Username already taken".to_owned());
                page.username.success = None;
                None
            }
        }
    } else {
        None
    };
    if id.is_some() {
        Err(Redirect::to(uri!(drivers)))
    } else {
        Ok(AppPage::with_template("drivers/register", page))
    }
}
