use serde_derive::Serialize;

#[derive(Serialize, Default)]
pub struct FormValue {
    pub value: Option<String>,
    pub error: Option<String>,
    pub success: Option<String>,
}
