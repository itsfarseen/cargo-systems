use once_cell::sync::OnceCell;
use serde_derive::{Deserialize, Serialize};
use std::{fs::File, io::Read};

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct Config {
    pub company_name: String,
    pub company_address: Vec<String>,
    pub vat_pct: f64,
    pub empost: Option<EmpostConfig>,
}

#[derive(Deserialize, Serialize, Clone, Debug)]
pub struct EmpostConfig {
    pub vat_pct: f64,
    pub empost_pct: f64,
    pub min_delivery_charge: f64,
    pub max_weight: f64,
}

static CONFIG: OnceCell<Config> = OnceCell::new();

impl Config {
    pub fn load() {
        let mut f = File::open("Config.toml").unwrap();
        let mut s = String::new();
        f.read_to_string(&mut s).unwrap();
        let config: Config = toml::from_str(&s).unwrap();
        CONFIG.set(config).unwrap();
    }

    pub fn get() -> &'static Config {
        CONFIG
            .get()
            .expect("Config::load() needs to be called once before Config::get().")
    }
}
