#![feature(decl_macro)]

#[macro_use]
extern crate rocket;

mod common;
mod login;
mod logout;

use rocket::Route;

pub fn routes() -> Vec<Route> {
    let mut routes = Vec::new();
    routes.append(&mut login::routes());
    routes.append(&mut logout::routes());
    routes
}
