use super::common::LoginResponse;
use rocket::{
    http::Cookies,
    response::{Flash, Redirect},
    Route,
};
use view_base::session::UserSession;

pub fn routes() -> Vec<Route> {
    routes![logout]
}

#[get("/logout")]
fn logout(mut cookies: Cookies) -> LoginResponse {
    UserSession::logout(&mut cookies);
    LoginResponse::FlashRedirect(Flash::success(
        Redirect::to("/login"),
        "Logged out successfully.",
    ))
}
