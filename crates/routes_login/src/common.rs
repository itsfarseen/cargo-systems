use rocket::response::{Flash, Redirect};
use serde_derive::Serialize;
use view_base::app_page::AppPage;

#[derive(Responder)]
pub(crate) enum LoginResponse {
    Redirect(Redirect),
    Page(AppPage<LoginPage<'static>>),
    FlashRedirect(Flash<Redirect>),
}

#[derive(Serialize, Default, Debug)]
pub(crate) struct LoginPage<'a> {
    pub title: &'a str,
    pub form_action: &'a str,
    pub error_message: Option<String>,
    pub info_message: Option<String>,
}
