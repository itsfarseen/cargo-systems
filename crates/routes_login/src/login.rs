use rocket::http::Cookies;
use rocket::request::{FlashMessage, Form};
use rocket::response::{Flash, Redirect};
use rocket::Route;

use crate::common::{LoginPage, LoginResponse};
use database::user::password_utils::password_check;
use database::user::User;
use view_base::app_page::AppPage;
use view_base::session::UserSession;
use db_provider::Db;

pub fn routes() -> Vec<Route> {
    routes![login, login_post]
}

#[derive(FromForm)]
struct LoginForm {
    pub username: String,
    pub password: String,
}

#[get("/login?<redir>")]
fn login(
    session: Option<UserSession>,
    flash: Option<FlashMessage>,
    redir: Option<String>,
) -> LoginResponse {
    if session.is_some() {
        return LoginResponse::Redirect(Redirect::to(redir.unwrap_or_else(|| "/".to_owned())));
    }
    let mut login_page = LoginPage {
        title: "Login",
        form_action: "/login",
        ..LoginPage::default()
    };

    if let Some(flash) = flash {
        if flash.name().eq("error") {
            login_page.error_message = Some(flash.msg().into());
        } else {
            login_page.info_message = Some(flash.msg().into());
        }
    }

    LoginResponse::Page(AppPage::with_template("login", login_page))
}

#[post("/login?<redir>", data = "<login>")]
fn login_post(
    db: Db,
    mut cookies: Cookies,
    redir: Option<String>,
    login: Form<LoginForm>,
) -> LoginResponse {
    match User::by_username(&login.username, &db.0) {
        Some(user) if password_check(&login.password, &user.hashword) => {
            UserSession::login(&mut cookies, &user);
            LoginResponse::Redirect(Redirect::to(redir.unwrap_or_else(|| "/".into())))
        }
        _ => LoginResponse::FlashRedirect(Flash::error(
            if let Some(redir) = redir {
                Redirect::to(format!("/login?redir={}", redir))
            } else {
                Redirect::to("/login")
            },
            "Invalid user id or password. Please try again.",
        )),
    }
}
