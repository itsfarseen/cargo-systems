use database::invoice::Invoice;
use database::user::UserRole;
use database::IdType;
use db_provider::Db;
use rocket::response::{Redirect, Responder};
use view_base::session::AdminSession;

#[post("/invoices/<id>?delete")]
pub(crate) fn post(session: AdminSession, db: Db, id: IdType) -> impl Responder<'static> {
    let invoice = Invoice::by_id(id, &db.0)?;

    let redirect_id = match session.user_role {
        UserRole::Admin => Some(invoice.client_id),
        UserRole::Client => {
            if invoice.client_id != session.id {
                return None;
            }
            None
        }
        _ => return None,
    };

    invoice.delete(&db);

    match redirect_id {
        Some(id) => Some(Redirect::to(format!("/invoices?client={}", id))),
        None => Some(Redirect::to("/invoices".to_string())),
    }
}
