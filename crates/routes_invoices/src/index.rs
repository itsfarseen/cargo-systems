use database::invoice::Invoice;
use database::user::{User, UserRole};
use database::IdType;
use rocket::response::Responder;
use view_base::app_page::AppPage;
use view_base::session::UserSession;
use db_provider::Db;

#[derive(Serialize)]
struct Page {
    is_admin: bool,
    client_id: i32,
    client_name: String,
    invoices: Vec<Invoice>,
}

#[get("/invoices?<client>")]
pub(crate) fn get(session: UserSession, db: Db, client: Option<IdType>) -> impl Responder<'static> {
    let client_id = {
        match session.user_role {
            UserRole::Admin => client?,
            UserRole::Client => {
                if client.is_some() {
                    return None;
                } else {
                    session.id
                }
            }
            _ => return None,
        }
    };

    let client = User::by_id(client_id, &db.0)?;
    let invoices = Invoice::by_client(client_id, &db.0);

    Some(AppPage::with_template(
        "invoices/index",
        Page {
            client_id: client.id,
            client_name: client.name,
            is_admin: session.user_role == UserRole::Admin,
            invoices,
        },
    ))
}
