use database::shipment::shipment_extra::{LoadWithStatuses, SearchParams};
use database::shipment::status_values::{StatusFilter, STATUS_FILTER_ALL};
use database::shipment::{shipment_extra, Shipment, ShipmentStatus};
use database::user::{User, UserRole};
use database::IdType;
use db_provider::Db;
use logic::invoice;
use rocket::{
    request::{Form, FormItems, FromForm},
    response::{Redirect, Responder},
    Rocket, State,
};
use std::{
    collections::HashMap,
    sync::{Arc, Mutex},
};
use view_base::app_page::AppPage;
use view_base::session::AdminSession;
use view_base::utils::parse_js_date;

pub fn setup(rocket: Rocket) -> Rocket {
    rocket.manage(StagedShipments(Default::default()))
}

#[derive(Serialize)]
enum FocusTab {
    Staging,
    Search,
}

pub(crate) struct StagedShipments(Arc<Mutex<HashMap<IdType, Vec<IdType>>>>);

impl StagedShipments {
    fn get_ids(&self, client_id: IdType) -> Vec<IdType> {
        let id_list = {
            let invoice_staging_list = self.0.clone();
            let invoice_list_lock = invoice_staging_list.lock().unwrap();
            invoice_list_lock
                .get(&client_id)
                .cloned()
                .unwrap_or_default()
        };
        id_list
    }

    fn update(&self, client_id: IdType, shipments: &mut dyn Iterator<Item = &Shipment>) {
        let id_list: Vec<i32> = shipments.map(|x| x.id).collect();
        let mut invoice_list_lock = self.0.lock().unwrap();
        invoice_list_lock.insert(client_id, id_list);
    }

    fn clear(&self, client_id: IdType) {
        let mut invoice_list_lock = self.0.lock().unwrap();
        invoice_list_lock.remove(&client_id);
    }
}

#[derive(Serialize)]
pub(crate) struct Page {
    client_id: i32,
    client_name: String,
    from_date: Option<String>,
    to_date: Option<String>,
    shipments_filters: &'static [StatusFilter],
    shipments_filter: Option<String>,
    search_results: Vec<(Shipment, Option<ShipmentStatus>)>,
    staged_shipments: Vec<(Shipment, Option<ShipmentStatus>)>,
    focus_tab: Option<FocusTab>,
    msg: Option<String>,
}

#[get("/invoices/new?<client>&<from_date>&<to_date>&<filter>", rank = 3)]
pub(crate) fn get(
    db: Db,
    _session: AdminSession,
    client: IdType,
    from_date: Option<String>,
    to_date: Option<String>,
    filter: Option<String>,
    staged_shipments_state: State<StagedShipments>,
) -> impl Responder<'static> {
    let client = User::by_id(client, &db.0)?;
    if client.user_role != UserRole::Client && client.user_role != UserRole::Admin {
        return None;
    }

    let staged_ids = staged_shipments_state.get_ids(client.id);
    let staged_shipments = Shipment::by_ids(&staged_ids, &db).load_last_status(&db);

    let empty_page = Page {
        client_id: client.id,
        client_name: client.name,
        from_date: from_date.clone(),
        to_date: to_date.clone(),
        shipments_filters: &STATUS_FILTER_ALL,
        shipments_filter: filter.clone(),
        search_results: Vec::new(),
        staged_shipments,
        focus_tab: None,
        msg: None,
    };

    if from_date.is_none() && to_date.is_none() && filter.is_none() {
        return Some(AppPage::with_template("invoices/new", empty_page));
    }

    match SearchParams::try_parse(&from_date, &to_date, &filter, parse_js_date) {
        Ok(search_params) => {
            let filter_fn = |s: &Shipment| s.invoice_id.is_none() && !staged_ids.contains(&s.id);
            let search_results = shipment_extra::load_shipments(&search_params, filter_fn, &db);
            let msg = if search_results.is_empty() {
                Some("No shipments found for given search".to_string())
            } else {
                None
            };
            Some(AppPage::with_template(
                "invoices/new",
                Page {
                    search_results,
                    msg,
                    focus_tab: Some(FocusTab::Search),
                    ..empty_page
                },
            ))
        }
        Err(msg) => Some(AppPage::with_template(
            "invoices/new",
            Page {
                msg: Some(msg),
                focus_tab: Some(FocusTab::Search),
                ..empty_page
            },
        )),
    }
}

pub(crate) struct ShipmentsSelectionForm {
    selected: Vec<IdType>,
}
impl<'f> FromForm<'f> for ShipmentsSelectionForm {
    type Error = ();

    fn from_form(items: &mut FormItems<'f>, _strict: bool) -> Result<Self, Self::Error> {
        let mut selected = Vec::new();

        for item in items {
            if item.key != "selected" {
                return Err(());
            }
            let decoded_value = item.value.url_decode().map_err(|_| ())?;
            let id = decoded_value.parse().map_err(|_| ())?;
            selected.push(id);
        }

        Ok(ShipmentsSelectionForm { selected })
    }
}

#[post(
    "/invoices/new?<client>&<from_date>&<to_date>&<filter>&action=add",
    data = "<form_items>",
    rank = 2
)]
pub(crate) fn post_add(
    client: IdType,
    _session: AdminSession,
    from_date: Option<String>,
    to_date: Option<String>,
    filter: Option<String>,
    staged_shipments_state: State<StagedShipments>,
    form_items: Form<ShipmentsSelectionForm>,
    db: Db,
) -> impl Responder<'static> {
    let client = User::by_id(client, &db)?;
    if client.user_role != UserRole::Client && client.user_role != UserRole::Admin {
        return None;
    }

    let mut selected = form_items.0.selected;
    let selected_len = selected.len();

    let mut staged_ids = staged_shipments_state.get_ids(client.id);
    staged_ids.append(&mut selected);

    let shipments = Shipment::by_ids(&staged_ids, &db);
    staged_shipments_state.update(client.id, &mut shipments.iter());

    let shipments_status = shipments.load_last_status(&db);

    let msg = format!("{} shipments added to list.", selected_len);

    let page = Page {
        client_id: client.id,
        client_name: client.name,
        from_date,
        to_date,
        shipments_filters: &STATUS_FILTER_ALL,
        shipments_filter: filter,
        search_results: Vec::new(),
        staged_shipments: shipments_status,
        focus_tab: Some(FocusTab::Staging),
        msg: Some(msg),
    };

    Some(AppPage::with_template("invoices/new", page))
}

#[post(
    "/invoices/new?<client>&<from_date>&<to_date>&<filter>&action=remove",
    data = "<form_items>",
    rank = 1
)]
pub(crate) fn post_remove(
    client: IdType,
    _session: AdminSession,
    from_date: Option<String>,
    to_date: Option<String>,
    filter: Option<String>,
    staged_shipments_state: State<StagedShipments>,
    form_items: Form<ShipmentsSelectionForm>,
    db: Db,
) -> impl Responder<'static> {
    let client = User::by_id(client, &db)?;
    if client.user_role != UserRole::Client && client.user_role != UserRole::Admin {
        return None;
    }

    let selected = form_items.0.selected;
    let selected_len = selected.len();

    let mut staged_ids = staged_shipments_state.get_ids(client.id);
    staged_ids.retain(|x| !selected.contains(x));

    let shipments = Shipment::by_ids(&staged_ids, &db);
    staged_shipments_state.update(client.id, &mut shipments.iter());

    let shipments_status = shipments.load_last_status(&db);

    let msg = format!("{} shipments removed from list.", selected_len);

    let page = Page {
        client_id: client.id,
        client_name: client.name,
        from_date,
        to_date,
        shipments_filters: &STATUS_FILTER_ALL,
        shipments_filter: filter,
        search_results: Vec::new(),
        staged_shipments: shipments_status,
        focus_tab: Some(FocusTab::Staging),
        msg: Some(msg),
    };

    Some(AppPage::with_template("invoices/new", page))
}

#[post("/invoices/new?<client>&action=generate", rank = 0)]
pub(crate) fn post_generate(
    client: IdType,
    _session: AdminSession,
    staged_shipments_state: State<StagedShipments>,
    db: Db,
) -> impl Responder<'static> {
    let client = User::by_id(client, &db.0)?;
    let staged_ids = staged_shipments_state.get_ids(client.id);

    let shipments = Shipment::by_ids(&staged_ids, &db);
    let shipments_status: Vec<(Shipment, Option<ShipmentStatus>)> = shipments.load_last_status(&db);

    let invoice = invoice::calculate(client.id, &shipments_status);
    let id = invoice.save(&db);
    Shipment::update_invoice(&staged_ids, Some(id), &db.0);

    staged_shipments_state.clear(client.id);
    Some(Redirect::to(uri!(crate::index::get: client.id)))
}
