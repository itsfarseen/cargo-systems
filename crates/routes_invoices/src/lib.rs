#![feature(decl_macro)]

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate serde_derive;

use rocket::{Rocket, Route};

mod delete;
mod index;
mod item;
mod new;

pub fn routes() -> Vec<Route> {
    routes![
        index::get,
        delete::post,
        item::get,
        new::get,
        new::post_add,
        new::post_remove,
        new::post_generate
    ]
}

pub fn setup(rocket: Rocket) -> Rocket {
    new::setup(rocket)
}
