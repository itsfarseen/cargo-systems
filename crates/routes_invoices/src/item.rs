use database::invoice::Invoice;
use database::shipment::shipment_extra::LoadWithStatuses;
use database::shipment::{Shipment, ShipmentStatus};
use database::user::{User, UserRole};
use database::IdType;
use db_provider::Db;
use rocket::response::Responder;
use rocket_contrib::templates::Template;
use view_base::app_page::AppPage;
use view_base::session::UserSession;

#[derive(Serialize)]
struct Page {
    client_name: String,
    shipments: Vec<(Shipment, Option<ShipmentStatus>)>,
    invoice: Invoice,
}

#[derive(Responder)]
enum Response {
    Page(AppPage<Page>),
    Print(Template),
}

#[get("/invoices/<id>?<print>")]
pub(crate) fn get(
    session: UserSession,
    db: Db,
    id: IdType,
    print: Option<String>,
) -> impl Responder<'static> {
    let invoice = Invoice::by_id(id, &db)?;

    match session.user_role {
        UserRole::Admin => {}
        UserRole::Client => {
            if invoice.client_id != session.id {
                return None;
            }
        }
        _ => return None,
    }

    let client = User::by_id(invoice.client_id, &db)?;
    let shipments = Shipment::by_invoice(invoice.id, &db).load_last_status(&db);

    let page = Page {
        client_name: client.name,
        shipments,
        invoice,
    };

    if print.is_none() {
        Some(Response::Page(AppPage::with_template(
            "invoices/item",
            page,
        )))
    } else {
        Some(Response::Print(Template::render("invoices/print", page)))
    }
}
