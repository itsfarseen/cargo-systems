use super::super::invoice::Invoice;
use super::super::schema::shipments;
use super::super::user::User;
use super::super::IdType;
use chrono::{NaiveDate, NaiveDateTime};
use diesel::pg::expression::dsl::any as pg_any;
use diesel::prelude::*;
use serde_derive::Serialize;

#[derive(Clone, Serialize, Debug, Queryable, Identifiable, Associations)]
#[belongs_to(User, foreign_key = "client_id")]
#[belongs_to(Invoice)]
pub struct Shipment {
    pub id: IdType,
    pub client_id: IdType,
    pub date_time: NaiveDateTime,
    pub your_ref: String,
    pub receiver_name: String,
    pub receiver_phone: String,
    pub receiver_addr: String,
    pub receiver_city: String,
    pub receiver_area: String,
    pub weight: f64,
    pub pieces: i32,
    pub material_cost: f64,
    pub spl_instrs: String,
    pub remarks: String,
    pub your_company: String,
    pub invoice_id: Option<IdType>,
    pub sender_name: String,
    pub sender_addr: String,
    pub sender_phone: String,
    pub delivery_charge: f64,
    pub return_charge: f64,
    pub empost_id: Option<IdType>,
}

impl Shipment {
    pub fn by_id(id: IdType, conn: &PgConnection) -> Option<Shipment> {
        shipments::table
            .filter(shipments::dsl::id.eq(id))
            .first(conn)
            .ok()
    }

    pub fn by_ids(ids: &[IdType], conn: &PgConnection) -> Vec<Shipment> {
        shipments::table
            .filter(shipments::dsl::id.eq(pg_any(ids)))
            .load(conn)
            .unwrap()
    }

    pub fn by_ref(client_id: IdType, your_ref: &str, conn: &PgConnection) -> Option<Shipment> {
        shipments::table
            .filter(shipments::dsl::client_id.eq(client_id))
            .filter(shipments::dsl::your_ref.eq(your_ref))
            .first(conn)
            .ok()
    }

    // date range is inclusive of both ends.
    // date_range.0 + 00:00:00 to date_range.1 + 23:59:59
    pub fn by_client_and_date_range(
        client_id: IdType,
        date_range: (NaiveDate, NaiveDate),
        conn: &PgConnection,
    ) -> Vec<Shipment> {
        shipments::table
            .filter(shipments::dsl::client_id.eq(client_id))
            .filter(shipments::dsl::date_time.ge(date_range.0.and_hms(00, 00, 00)))
            .filter(shipments::dsl::date_time.le(date_range.1.and_hms(23, 59, 59)))
            .load(conn)
            .unwrap()
    }

    pub fn by_date_range(date_range: (NaiveDate, NaiveDate), conn: &PgConnection) -> Vec<Shipment> {
        shipments::table
            .filter(shipments::dsl::date_time.ge(date_range.0.and_hms(00, 00, 00)))
            .filter(shipments::dsl::date_time.le(date_range.1.and_hms(23, 59, 59)))
            .load(conn)
            .unwrap()
    }

    pub fn by_invoice(invoice_id: IdType, conn: &PgConnection) -> Vec<Shipment> {
        shipments::table
            .filter(shipments::dsl::invoice_id.eq(invoice_id))
            .load(conn)
            .unwrap()
    }

    pub fn update_invoice(ids: &[IdType], invoice_id: Option<IdType>, conn: &PgConnection) {
        diesel::update(shipments::table)
            .filter(shipments::dsl::id.eq(pg_any(ids)))
            .set(shipments::invoice_id.eq(invoice_id))
            .execute(conn)
            .unwrap();
    }

    pub fn by_empost(empost_id: IdType, conn: &PgConnection) -> Vec<Shipment> {
        shipments::table
            .filter(shipments::dsl::empost_id.eq(empost_id))
            .load(conn)
            .unwrap()
    }

    pub fn update_empost(ids: &[IdType], empost_id: Option<IdType>, conn: &PgConnection) {
        diesel::update(shipments::table)
            .filter(shipments::dsl::id.eq(pg_any(ids)))
            .set(shipments::empost_id.eq(empost_id))
            .execute(conn)
            .unwrap();
    }

    pub fn by_sender_phone_last(phone: &str, conn: &PgConnection) -> Option<Shipment> {
        shipments::table
            .filter(shipments::dsl::sender_phone.ilike(phone))
            .first(conn)
            .ok()
    }

    pub fn by_receiver_phone_last(phone: &str, conn: &PgConnection) -> Option<Shipment> {
        shipments::table
            .filter(shipments::dsl::receiver_phone.ilike(phone))
            .first(conn)
            .ok()
    }
}

#[derive(Insertable)]
#[table_name = "shipments"]
pub struct NewShipment {
    pub client_id: IdType,
    pub date_time: NaiveDateTime,
    pub your_ref: String,
    pub receiver_name: String,
    pub receiver_phone: String,
    pub receiver_addr: String,
    pub receiver_city: String,
    pub receiver_area: String,
    pub weight: f64,
    pub pieces: i32,
    pub material_cost: f64,
    pub spl_instrs: String,
    pub remarks: String,
    pub your_company: String,
    pub invoice_id: Option<IdType>,
    pub sender_name: String,
    pub sender_addr: String,
    pub sender_phone: String,
    pub delivery_charge: f64,
    pub return_charge: f64,
}

impl NewShipment {
    pub fn save(&self, conn: &PgConnection) -> IdType {
        diesel::insert_into(shipments::table)
            .values(self)
            .returning(shipments::id)
            .get_result(conn)
            .unwrap()
    }

    pub fn save_many(items: &[NewShipment], conn: &PgConnection) -> Vec<IdType> {
        diesel::insert_into(shipments::table)
            .values(items)
            .returning(shipments::id)
            .get_results(conn)
            .unwrap()
    }
}
