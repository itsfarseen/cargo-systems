use super::super::schema::shipment_status;
use super::super::user::{GetUserId, User};
use super::super::IdType;
use super::Shipment;
use chrono::NaiveDateTime;

use diesel::prelude::*;
use serde_derive::Serialize;

#[derive(Clone, Serialize, Debug, Queryable, Identifiable, Associations)]
#[table_name = "shipment_status"]
#[belongs_to(Shipment)]
#[belongs_to(User)]
pub struct ShipmentStatus {
    pub id: IdType,
    pub shipment_id: IdType,
    pub date_time: NaiveDateTime,
    pub location: String,
    pub status: String,
    pub remarks: String,
    pub user_id: IdType,
}

impl ShipmentStatus {
    pub fn by_id(id: IdType, conn: &PgConnection) -> Option<Self> {
        shipment_status::table
            .filter(shipment_status::id.eq(id))
            .first(conn)
            .ok()
    }

    pub fn by_shipment_last_status(shipment: &Shipment, conn: &PgConnection) -> Option<Self> {
        ShipmentStatus::belonging_to(shipment)
            .order_by(shipment_status::date_time.desc())
            .then_order_by(shipment_status::id.desc())
            .first(conn)
            .ok()
    }

    pub fn by_shipment_all_status(shipment: &Shipment, conn: &PgConnection) -> Vec<Self> {
        ShipmentStatus::belonging_to(shipment)
            .order_by(shipment_status::date_time.desc())
            .then_order_by(shipment_status::id.desc())
            .load(conn)
            .unwrap()
    }

    pub fn by_shipments_last_status(
        shipments: &[Shipment],
        conn: &PgConnection,
    ) -> Vec<Option<Self>> {
        ShipmentStatus::belonging_to(shipments)
            // asc() because we are popping and getting the last element below
            .order_by(shipment_status::date_time.asc())
            .then_order_by(shipment_status::id.asc())
            .load(conn)
            .unwrap()
            .grouped_by(shipments)
            .into_iter()
            .map(|mut x| x.pop())
            .collect()
    }

    pub fn by_shipments_all_status(shipments: &[Shipment], conn: &PgConnection) -> Vec<Vec<Self>> {
        ShipmentStatus::belonging_to(shipments)
            .order_by(shipment_status::date_time.desc())
            .then_order_by(shipment_status::id.desc())
            .load(conn)
            .unwrap()
            .grouped_by(shipments)
    }
}

impl GetUserId for ShipmentStatus {
    fn get_user_id(&self) -> IdType {
        self.user_id
    }
}

#[derive(Clone, Serialize, Debug, Insertable)]
#[table_name = "shipment_status"]
pub struct NewShipmentStatus {
    pub shipment_id: IdType,
    pub date_time: NaiveDateTime,
    pub location: String,
    pub status: String,
    pub remarks: String,
    pub user_id: IdType,
}

impl NewShipmentStatus {
    pub fn save(&self, conn: &PgConnection) -> IdType {
        diesel::insert_into(shipment_status::table)
            .values(self)
            .returning(shipment_status::id)
            .get_result(conn)
            .unwrap()
    }

    pub fn save_many(items: &[Self], conn: &PgConnection) -> Vec<IdType> {
        diesel::insert_into(shipment_status::table)
            .values(items)
            .returning(shipment_status::id)
            .get_results(conn)
            .unwrap()
    }
}
