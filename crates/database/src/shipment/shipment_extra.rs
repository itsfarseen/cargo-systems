use chrono::NaiveDate;
use diesel::PgConnection;

use super::status_values::StatusFilter;
use super::{Shipment, ShipmentStatus};

pub struct SearchParams {
    pub from_date: NaiveDate,
    pub to_date: NaiveDate,
    pub status_filter: StatusFilter,
}

impl SearchParams {
    pub fn try_parse(
        from_date: &Option<String>,
        to_date: &Option<String>,
        status_filter: &Option<String>,
        date_parser_fn: fn(&str) -> Option<NaiveDate>,
    ) -> Result<Self, String> {
        let from_date = match from_date {
            Some(x) => x,
            None => return Err("Please specify From date".to_string()),
        };
        let to_date = match to_date {
            Some(x) => x,
            None => return Err("Please specify To date".to_string()),
        };
        let status_filter = match status_filter {
            Some(x) => x,
            None => return Err("Please specify Filter".to_string()),
        };

        let from_date = match date_parser_fn(from_date) {
            Some(x) => x,
            None => return Err("Invalid From date".to_string()),
        };
        let to_date = match date_parser_fn(to_date) {
            Some(x) => x,
            None => return Err("Invalid To date".to_string()),
        };
        let status_filter = match StatusFilter::from_str(status_filter) {
            Some(x) => x,
            None => return Err("Invalid Filter".to_string()),
        };
        Ok(Self {
            from_date,
            to_date,
            status_filter,
        })
    }
}

pub fn load_shipments(
    search_params: &SearchParams,
    filter: impl Fn(&Shipment) -> bool,
    db: &PgConnection,
) -> Vec<(Shipment, Option<ShipmentStatus>)> {
    let SearchParams {
        from_date,
        to_date,
        status_filter,
        ..
    } = search_params;

    let shipments = Shipment::by_date_range((*from_date, *to_date), &db);
    let shipments: Vec<Shipment> = shipments
        .into_iter()
        // Filter before loading statuses
        .filter(filter)
        .collect();
    let statuses = ShipmentStatus::by_shipments_last_status(&shipments, &db);

    shipments
        .into_iter()
        .zip(statuses.into_iter())
        .filter(|(_shipment, status)| {
            status
                .as_ref()
                .map(|status| status_filter.check(&status.status))
                .unwrap_or(false)
        })
        .collect()
}

pub trait LoadWithStatuses {
    fn load_last_status(self, db: &PgConnection) -> Vec<(Shipment, Option<ShipmentStatus>)>;
    fn load_all_status(self, db: &PgConnection) -> Vec<(Shipment, Vec<ShipmentStatus>)>;
}

impl LoadWithStatuses for Vec<Shipment> {
    fn load_last_status(self, db: &PgConnection) -> Vec<(Shipment, Option<ShipmentStatus>)> {
        let statuses = ShipmentStatus::by_shipments_last_status(&self, db);
        self.into_iter().zip(statuses.into_iter()).collect()
    }

    fn load_all_status(self, db: &PgConnection) -> Vec<(Shipment, Vec<ShipmentStatus>)> {
        let statuses = ShipmentStatus::by_shipments_all_status(&self, db);
        self.into_iter().zip(statuses.into_iter()).collect()
    }
}
