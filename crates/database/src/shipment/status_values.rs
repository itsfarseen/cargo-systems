pub const ALL: &[&str] = &[
    BOOKED,
    CHECKED_IN,
    IN_TRANSIT,
    WAITING_REMOTE_AREA,
    OUT_FOR_DELIVERY,
    DELIVERED,
    RETURNED_TO_OFFICE,
    RETURNED_TO_CLIENT,
    CANCELLED,
    DELIVERED_NOT_PAID,
];

pub const DELIVERED_ALL: [&str; 2] = [DELIVERED, DELIVERED_NOT_PAID];

pub const RETURNED_ALL: [&str; 2] = [RETURNED_TO_OFFICE, RETURNED_TO_CLIENT];

pub const BOOKED: &str = "Booked";
pub const CHECKED_IN: &str = "Checked in";
pub const IN_TRANSIT: &str = "In transit";
pub const WAITING_REMOTE_AREA: &str = "Waiting for remote area delivery";
pub const OUT_FOR_DELIVERY: &str = "Out for delivery";
pub const DELIVERED: &str = "Delivered";
pub const RETURNED_TO_OFFICE: &str = "Returned to office/hub";
pub const RETURNED_TO_CLIENT: &str = "Returned to client";
pub const CANCELLED: &str = "Cancelled";
pub const DELIVERED_NOT_PAID: &str = "Delivered, Not Paid";

#[derive(Copy, Clone, Serialize, Deserialize, Debug, Eq, PartialEq)]
pub enum StatusFilter {
    #[serde(rename(
        serialize = "Delivered / Returned",
        deserialize = "Delivered / Returned"
    ))]
    DeliveredReturned,
    Delivered,
    Returned,
    Others,
    All,
}

pub const STATUS_FILTER_ALL: [StatusFilter; 5] = [
    StatusFilter::DeliveredReturned,
    StatusFilter::Delivered,
    StatusFilter::Returned,
    StatusFilter::Others,
    StatusFilter::All,
];

impl StatusFilter {
    pub fn to_string(&self) -> String {
        serde_plain::to_string(self).unwrap()
    }

    pub fn from_str(s: &str) -> Option<Self> {
        serde_plain::from_str(s).ok()
    }

    pub fn check(&self, s: &str) -> bool {
        match self {
            StatusFilter::All => true,
            StatusFilter::Delivered => DELIVERED_ALL.contains(&s),
            StatusFilter::Returned => RETURNED_ALL.contains(&s),
            StatusFilter::DeliveredReturned => {
                StatusFilter::Delivered.check(s) || StatusFilter::Returned.check(s)
            }
            StatusFilter::Others => !StatusFilter::DeliveredReturned.check(s),
        }
    }
}

#[test]
fn test_status_filter_serialize() {
    for filter in &STATUS_FILTER_ALL {
        let s = filter.to_string();
        let filter1 = StatusFilter::from_str(&s);
        assert_eq!(filter1.as_ref(), Some(filter));
    }
}
