use super::super::schema::emposts;
use super::super::IdType;
use chrono::{NaiveDate, NaiveDateTime};
use diesel::prelude::*;
use serde_derive::Serialize;

#[derive(Clone, Serialize, Debug, Queryable, Identifiable, AsChangeset, Associations)]
pub struct Empost {
    pub id: IdType,
    pub date_time: NaiveDateTime,
    pub from_date: NaiveDate,
    pub to_date: NaiveDate,
    pub notes: String,

    pub vat_pct: f64,
    pub empost_pct: f64,

    pub empost_min_delivery_charge: f64,
    pub empost_max_weight: f64,

    pub num_shipments: i32,

    pub above_weight_num_shipments: i32,
    pub above_weight_sum_delivery_charge: f64,
    pub above_weight_empost_amt: f64,
    pub above_weight_empost_vat: f64,

    pub below_min_charge_num_shipments: i32,
    pub below_min_charge_sum_delivery_charge: f64,
    pub below_min_charge_empost_amt: f64,
    pub below_min_charge_empost_vat: f64,

    pub other_num_shipments: i32,
    pub other_sum_delivery_charge: f64,
    pub other_empost_amt: f64,
    pub other_empost_vat: f64,

    pub sum_delivery_charge: f64,
    pub sum_empost_amt: f64,
    pub sum_empost_vat: f64,
}

impl Empost {
    pub fn all(conn: &PgConnection) -> Vec<Self> {
        emposts::table.load(conn).unwrap()
    }

    pub fn by_id(id: IdType, conn: &PgConnection) -> Option<Self> {
        emposts::table
            .filter(emposts::dsl::id.eq(id))
            .first(conn)
            .ok()
    }

    pub fn delete(&self, conn: &PgConnection) {
        diesel::delete(emposts::table.filter(emposts::dsl::id.eq(self.id)))
            .execute(conn)
            .unwrap();
    }
}

#[derive(Clone, Serialize, Debug, Insertable)]
#[table_name = "emposts"]
pub struct NewEmpost {
    pub date_time: NaiveDateTime,
    pub from_date: NaiveDate,
    pub to_date: NaiveDate,
    pub notes: String,

    pub vat_pct: f64,
    pub empost_pct: f64,

    pub empost_min_delivery_charge: f64,
    pub empost_max_weight: f64,

    pub num_shipments: i32,

    pub above_weight_num_shipments: i32,
    pub above_weight_sum_delivery_charge: f64,
    pub above_weight_empost_amt: f64,
    pub above_weight_empost_vat: f64,

    pub below_min_charge_num_shipments: i32,
    pub below_min_charge_sum_delivery_charge: f64,
    pub below_min_charge_empost_amt: f64,
    pub below_min_charge_empost_vat: f64,

    pub other_num_shipments: i32,
    pub other_sum_delivery_charge: f64,
    pub other_empost_amt: f64,
    pub other_empost_vat: f64,

    pub sum_delivery_charge: f64,
    pub sum_empost_amt: f64,
    pub sum_empost_vat: f64,
}

impl NewEmpost {
    pub fn save(&self, conn: &PgConnection) -> IdType {
        diesel::insert_into(emposts::table)
            .values(self)
            .returning(emposts::id)
            .get_result(conn)
            .unwrap()
    }
}
