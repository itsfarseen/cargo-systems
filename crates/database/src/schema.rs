table! {
    use diesel::sql_types::*;

    client_info (client_id) {
        client_id -> Int4,
        delivery_charge -> Float8,
        return_charge -> Float8,
    }
}

table! {
    use diesel::sql_types::*;

    emposts (id) {
        id -> Int4,
        date_time -> Timestamp,
        from_date -> Date,
        to_date -> Date,
        notes -> Text,
        vat_pct -> Float8,
        empost_pct -> Float8,
        empost_min_delivery_charge -> Float8,
        empost_max_weight -> Float8,
        num_shipments -> Int4,
        above_weight_num_shipments -> Int4,
        above_weight_sum_delivery_charge -> Float8,
        above_weight_empost_amt -> Float8,
        above_weight_empost_vat -> Float8,
        below_min_charge_num_shipments -> Int4,
        below_min_charge_sum_delivery_charge -> Float8,
        below_min_charge_empost_amt -> Float8,
        below_min_charge_empost_vat -> Float8,
        other_num_shipments -> Int4,
        other_sum_delivery_charge -> Float8,
        other_empost_amt -> Float8,
        other_empost_vat -> Float8,
        sum_delivery_charge -> Float8,
        sum_empost_amt -> Float8,
        sum_empost_vat -> Float8,
    }
}

table! {
    use diesel::sql_types::*;

    invoices (id) {
        id -> Int4,
        client_id -> Int4,
        date_time -> Timestamp,
        notes -> Text,
        sum_material_cost -> Float8,
        vat_pct -> Float8,
        num_deliveries -> Int4,
        sum_delivery_charge -> Float8,
        num_returns -> Int4,
        sum_return_charge -> Float8,
        num_in_transit -> Int4,
        num_shipments -> Int4,
        invoice_amt -> Float8,
        net_payable -> Float8,
        vat_incl_sum_delivery_charge -> Float8,
        vat_incl_sum_return_charge -> Float8,
    }
}

table! {
    use diesel::sql_types::*;

    shipment_status (id) {
        id -> Int4,
        shipment_id -> Int4,
        date_time -> Timestamp,
        location -> Varchar,
        status -> Varchar,
        remarks -> Varchar,
        user_id -> Int4,
    }
}

table! {
    use diesel::sql_types::*;

    shipments (id) {
        id -> Int4,
        client_id -> Int4,
        date_time -> Timestamp,
        your_ref -> Varchar,
        receiver_name -> Varchar,
        receiver_phone -> Varchar,
        receiver_addr -> Varchar,
        receiver_city -> Varchar,
        receiver_area -> Varchar,
        weight -> Float8,
        pieces -> Int4,
        material_cost -> Float8,
        spl_instrs -> Varchar,
        remarks -> Varchar,
        your_company -> Varchar,
        invoice_id -> Nullable<Int4>,
        sender_name -> Text,
        sender_addr -> Text,
        sender_phone -> Text,
        delivery_charge -> Float8,
        return_charge -> Float8,
        empost_id -> Nullable<Int4>,
    }
}

table! {
    use diesel::sql_types::*;
    use crate::diesel_exports::*;

    users (id) {
        id -> Int4,
        name -> Varchar,
        username -> Varchar,
        hashword -> Varchar,
        user_role -> Userrole,
        address -> Text,
        phone -> Text,
    }
}

joinable!(client_info -> users (client_id));
joinable!(shipment_status -> shipments (shipment_id));
joinable!(shipment_status -> users (user_id));
joinable!(shipments -> emposts (empost_id));
joinable!(shipments -> invoices (invoice_id));
joinable!(shipments -> users (client_id));

allow_tables_to_appear_in_same_query!(
    client_info,
    emposts,
    invoices,
    shipment_status,
    shipments,
    users,
);
