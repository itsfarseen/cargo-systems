use diesel::PgConnection;
use user::{password_utils, NewUser, UserRole};

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;

#[macro_use]
extern crate diesel_derive_enum;
#[macro_use]
extern crate serde_derive;

pub mod empost;
pub mod invoice;
mod schema;
pub mod shipment;
pub mod user;

pub type IdType = i32;

pub mod diesel_exports {
    pub use super::user::diesel_exports::*;
}

embed_migrations!("../../migrations");

pub fn setup(conn: &PgConnection) {
    embedded_migrations::run_with_output(conn, &mut std::io::stdout()).unwrap();
}

pub fn reset_db(conn: &PgConnection) {
    use diesel::prelude::*;
    diesel::delete(schema::users::table).execute(conn).unwrap();
    diesel::delete(schema::client_info::table)
        .execute(conn)
        .unwrap();
    diesel::delete(schema::shipments::table)
        .execute(conn)
        .unwrap();
    diesel::delete(schema::shipment_status::table)
        .execute(conn)
        .unwrap();
    diesel::delete(schema::invoices::table)
        .execute(conn)
        .unwrap();
    diesel::delete(schema::emposts::table)
        .execute(conn)
        .unwrap();

    NewUser {
        name: "Admin".into(),
        username: "admin".into(),
        hashword: password_utils::password_hash("admin"),
        address: "".into(),
        phone: "".into(),
        user_role: UserRole::Admin,
    }
    .save(conn);
}
