use super::super::schema::invoices;
use super::super::user::User;
use super::super::IdType;
use chrono::NaiveDateTime;
use diesel::prelude::*;
use serde_derive::Serialize;

#[derive(Clone, Serialize, Debug, Queryable, Identifiable, AsChangeset, Associations)]
#[belongs_to(User, foreign_key = "client_id")]
pub struct Invoice {
    pub id: IdType,
    pub client_id: i32,
    pub date_time: NaiveDateTime,
    pub notes: String,

    pub sum_material_cost: f64,
    pub vat_pct: f64,

    pub num_deliveries: i32,
    pub sum_delivery_charge: f64,

    pub num_returns: i32,
    pub sum_return_charge: f64,

    pub num_in_transit: i32,

    pub num_shipments: i32,

    pub invoice_amt: f64,
    pub net_payable: f64,
    pub vat_incl_sum_delivery_charge: f64,
    pub vat_incl_sum_return_charge: f64,
}

impl Invoice {
    pub fn by_id(id: IdType, conn: &PgConnection) -> Option<Self> {
        invoices::table
            .filter(invoices::dsl::id.eq(id))
            .first(conn)
            .ok()
    }
    pub fn by_client(client_id: IdType, conn: &PgConnection) -> Vec<Self> {
        invoices::table
            .filter(invoices::dsl::client_id.eq(client_id))
            .load(conn)
            .unwrap()
    }

    pub fn delete(&self, conn: &PgConnection) {
        diesel::delete(invoices::table.filter(invoices::dsl::id.eq(self.id)))
            .execute(conn)
            .unwrap();
    }
}

#[derive(Clone, Serialize, Debug, Insertable)]
#[table_name = "invoices"]
pub struct NewInvoice {
    pub client_id: i32,
    pub date_time: NaiveDateTime,
    pub notes: String,

    pub sum_material_cost: f64,
    pub vat_pct: f64,

    pub num_deliveries: i32,
    pub sum_delivery_charge: f64,

    pub num_returns: i32,
    pub sum_return_charge: f64,

    pub num_in_transit: i32,

    pub num_shipments: i32,

    pub invoice_amt: f64,
    pub net_payable: f64,
    pub vat_incl_sum_delivery_charge: f64,
    pub vat_incl_sum_return_charge: f64,
}

impl NewInvoice {
    pub fn save(&self, conn: &PgConnection) -> IdType {
        diesel::insert_into(invoices::table)
            .values(self)
            .returning(invoices::id)
            .get_result(conn)
            .unwrap()
    }
}
