use serde_derive::Serialize;
#[derive(Clone, Serialize, Copy, Eq, PartialEq, Debug, DbEnum, Hash)]
#[PgType = "UserRole"]
pub enum UserRole {
    Admin,
    Client,
    Driver,
}

pub mod diesel_exports {
    pub use super::UserRoleMapping as Userrole;
}
