use super::UserRole;
use crate::schema::users;
use crate::IdType;
use diesel::pg::expression::dsl::any as pg_any;
use diesel::prelude::*;
use serde_derive::Serialize;

#[derive(Clone, Serialize, Queryable, Identifiable, AsChangeset)]
pub struct User {
    pub id: IdType,
    pub name: String,
    pub username: String,

    // we don't want to send hashword out through rocket
    #[serde(skip_serializing)]
    pub hashword: String,

    pub user_role: UserRole,
    pub address: String,
    pub phone: String,
}

impl User {
    pub fn by_id(id: IdType, conn: &PgConnection) -> Option<Self> {
        users::table.filter(users::dsl::id.eq(id)).first(conn).ok()
    }

    pub fn by_role(user_role: UserRole, conn: &PgConnection) -> Vec<Self> {
        users::table
            .filter(users::dsl::user_role.eq(user_role))
            .load(conn)
            .unwrap()
    }

    pub fn by_username<'a>(username: &'a str, conn: &PgConnection) -> Option<Self> {
        users::table
            .filter(users::dsl::username.eq(username))
            .first(conn)
            .ok()
    }

    pub fn by_ids(ids: &[IdType], conn: &PgConnection) -> Vec<Self> {
        users::table
            .filter(users::dsl::id.eq(pg_any(ids)))
            .load(conn)
            .unwrap()
    }

    pub fn update(&self, conn: &PgConnection) {
        diesel::update(self).set(self).execute(conn).unwrap();
    }
}

#[derive(Insertable)]
#[table_name = "users"]
pub struct NewUser {
    pub name: String,
    pub username: String,
    pub hashword: String,
    pub address: String,
    pub phone: String,
    pub user_role: UserRole,
}

impl NewUser {
    pub fn save(&self, conn: &PgConnection) -> Option<IdType> {
        self.insert_into(users::table)
            .returning(users::id)
            .get_result(conn)
            .ok()
    }
}
