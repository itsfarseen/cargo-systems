use pbkdf2::{pbkdf2_check, pbkdf2_simple};

pub fn password_hash(password: &str) -> String {
    pbkdf2_simple(password, 1).unwrap()
}

pub fn password_check(password: &str, hashword: &str) -> bool {
    pbkdf2_check(password, hashword).is_ok()
}
