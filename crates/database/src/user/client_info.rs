use super::User;
use crate::schema::client_info;
use crate::IdType;
use diesel::prelude::*;
use diesel::BelongingToDsl;
use diesel::PgConnection;
use serde_derive::Serialize;
#[derive(Clone, Serialize, Queryable, Insertable, Identifiable, Associations, AsChangeset)]
#[belongs_to(User, foreign_key = "client_id")]
#[primary_key(client_id)]
#[table_name = "client_info"]
pub struct ClientInfo {
    pub client_id: IdType,
    pub delivery_charge: f64,
    pub return_charge: f64,
}

impl ClientInfo {
    pub fn by_id(id: IdType, conn: &PgConnection) -> Option<Self> {
        client_info::table
            .filter(client_info::client_id.eq(id))
            .first(conn)
            .ok()
    }

    pub fn by_users(users: &[User], conn: &PgConnection) -> Vec<Option<Self>> {
        ClientInfo::belonging_to(users)
            .load(conn)
            .unwrap()
            .grouped_by(users)
            .iter_mut()
            .map(Vec::pop)
            .collect()
    }

    pub fn save(&self, conn: &PgConnection) -> IdType {
        self.insert_into(client_info::table)
            .returning(client_info::client_id)
            .on_conflict(client_info::client_id)
            .do_update()
            .set(self)
            .get_result(conn)
            .unwrap()
    }
}
