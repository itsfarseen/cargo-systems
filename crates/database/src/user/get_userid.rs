use super::super::IdType;
use std::collections::HashSet;

pub trait GetUserId {
    fn get_user_id(&self) -> IdType;
}

pub trait GetUserIds {
    fn get_user_ids(&self) -> HashSet<IdType>;
}

impl<T> GetUserIds for Vec<T>
where
    T: GetUserId,
{
    fn get_user_ids(&self) -> HashSet<IdType> {
        let mut set = HashSet::new();
        for s in self {
            set.insert(s.get_user_id());
        }
        set
    }
}

impl<T> GetUserIds for Vec<Option<T>>
where
    T: GetUserId,
{
    fn get_user_ids(&self) -> HashSet<IdType> {
        let mut set = HashSet::new();
        for s in self {
            if let Some(s) = s {
                set.insert(s.get_user_id());
            }
        }
        set
    }
}

impl<T> GetUserIds for Vec<Vec<T>>
where
    T: GetUserId,
{
    fn get_user_ids(&self) -> HashSet<IdType> {
        let mut set = HashSet::new();
        for ss in self {
            for s in ss {
                set.insert(s.get_user_id());
            }
        }
        set
    }
}
