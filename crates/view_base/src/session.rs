use std::str::FromStr;

use derive_deref::Deref;
use rocket::{http::Cookie, http::Cookies, request::FromRequest, Outcome};

use database::{user::User, user::UserRole};
use db_provider::Db;

static COOKIE_USER_ID: &str = "user_logged_in_id";

#[derive(Deref)]
pub struct UserSession(User);
#[derive(Deref)]
pub struct AdminSession(User);
#[derive(Deref)]
pub struct ClientSession(User);
#[derive(Deref)]
pub struct DriverSession(User);

impl UserSession {
    pub fn login(cookies: &mut Cookies, user: &User) {
        cookies.add_private(Cookie::new(COOKIE_USER_ID, user.id.to_string()));
    }

    pub fn logout(cookies: &mut Cookies) {
        cookies.remove_private(Cookie::named(COOKIE_USER_ID));
    }
}

impl<'a, 'r> FromRequest<'a, 'r> for UserSession {
    type Error = String;
    fn from_request(
        request: &'a rocket::Request<'r>,
    ) -> rocket::request::Outcome<Self, Self::Error> {
        let mut cookies: Cookies = request.guard().unwrap();
        let db: Db = request.guard().unwrap();

        let user_id = cookies
            .get_private(COOKIE_USER_ID)
            .and_then(|c| FromStr::from_str(c.value()).ok());

        let user = user_id.and_then(|id| User::by_id(id, &db.0));

        let self_ = user.map(UserSession);

        self_.map_or(Outcome::Forward(()), Outcome::Success)
    }
}

macro_rules! impl_session_role {
    ($guard_type:ty, $role:expr) => {
        impl<'a, 'r> FromRequest<'a, 'r> for $guard_type {
            type Error = String;
            fn from_request(
                request: &'a rocket::Request<'r>,
            ) -> rocket::request::Outcome<Self, Self::Error> {
                let user_session: UserSession = request.guard()?;
                if user_session.0.user_role == $role {
                    Outcome::Success(Self(user_session.0))
                } else {
                    Outcome::Forward(())
                }
            }
        }
    };
}

impl_session_role!(AdminSession, UserRole::Admin);
impl_session_role!(ClientSession, UserRole::Client);
impl_session_role!(DriverSession, UserRole::Driver);
