use rocket::Rocket;

pub mod app_page;
pub mod nav;
pub mod session;
pub mod utils;

use rocket_contrib::templates::Template;

pub fn setup(rocket: Rocket) -> Rocket {
    let config = config::Config::get();

    let template_fn = Box::new(move |_| {
        let res = serde_json::to_value(config).unwrap();
        Ok(res)
    });
    rocket.attach(Template::custom(move |engines| {
        engines
            .tera
            .register_function("config", template_fn.clone());
    }))
}
