use config::Config;
use database::user::UserRole;
use hmap::hmap;
use lazy_static::lazy_static;
use serde_derive::Serialize;
use std::collections::HashMap;

pub type Nav = Vec<NavEntry<'static>>;
#[derive(Serialize, Copy, Clone, Debug)]
pub struct NavEntry<'a> {
    pub order: isize,
    pub title: &'a str,
    pub href: &'a str,
    pub active: bool,
}

lazy_static! {
    pub static ref NAV_BY_ROLE: HashMap<UserRole, &'static Nav> = hmap!(UserRole::Admin => ADMIN_NAV.as_ref(), UserRole::Driver => DRIVER_NAV.as_ref(), UserRole::Client => CLIENT_NAV.as_ref());
    static ref ADMIN_NAV: Nav = define_nav(&{
        let mut nav = vec![
            ("Dashboard", "/"),
            ("Book", "/shipments/book"),
            ("Scan", "/shipments/scan"),
            ("Track", "/shipments/track"),
            ("Clients", "/clients"),
            ("Drivers", "/drivers"),
        ];
        let config = Config::get();
        if config.empost.is_some() {
            nav.push(("Empost", "/emposts"));
        }
        nav.push(("Logout", "/logout"));
        nav
    });
    static ref CLIENT_NAV: Nav = define_nav(&[
        ("Dashboard", "/"),
        ("Track", "/shipments/track"),
        ("Logout", "/logout")
    ]);
    static ref DRIVER_NAV: Nav = define_nav(&[
        ("Scan", "/shipments/scan"),
        ("Track", "/shipments/track"),
        ("Logout", "/logout")
    ]);
    pub static ref DEFAULT_NAV: Nav =
        define_nav(&[("Track", "/shipments/track"), ("Login", "/login")]);
}

fn define_nav(navs: &[(&'static str, &'static str)]) -> Nav {
    let mut nav: Nav = Vec::new();
    for (i, (title, ref route)) in navs.iter().enumerate() {
        nav.push(NavEntry {
            order: i as isize,
            title,
            href: route,
            active: false,
        });
    }
    nav
}
