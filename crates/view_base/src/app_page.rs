use super::nav::{Nav, DEFAULT_NAV, NAV_BY_ROLE};
use rocket::{response::Responder, Outcome};
use rocket_contrib::templates::Template;
use serde::Serialize;
use serde_derive::Serialize;

use crate::session::UserSession;

#[derive(Serialize, Clone, Debug)]
pub struct AppPage<Page: Serialize> {
    template_name: &'static str,
    nav: Nav,
    page: Page,
}

impl<Page: Serialize> AppPage<Page> {
    pub fn with_template(template_name: &'static str, page: Page) -> Self {
        Self {
            template_name,
            nav: Nav::default(),
            page,
        }
    }
}

impl<'r, Page: Serialize> Responder<'r> for AppPage<Page> {
    fn respond_to(mut self, request: &rocket::Request) -> rocket::response::Result<'r> {
        let user_session = request.guard();
        let mut nav_for_role = if let Outcome::Success(user_session) = user_session {
            let user_session: UserSession = user_session;
            (*NAV_BY_ROLE.get(&user_session.user_role).unwrap()).clone()
        } else {
            (*DEFAULT_NAV).clone()
        };
        self.nav = {
            let mut active_idx = None;
            for (i, nav) in nav_for_role.iter().enumerate() {
                if request.route().unwrap().uri.path().eq(nav.href) {
                    active_idx = Some(i);
                }
            }

            if let Some(i) = active_idx {
                nav_for_role[i].active = true;
            }
            nav_for_role
        };
        Template::render(self.template_name, self).respond_to(request)
    }
}
