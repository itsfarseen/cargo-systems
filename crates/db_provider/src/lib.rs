#[macro_use]
extern crate rocket_contrib;
use rocket::Rocket;
use rocket_contrib::databases::diesel;

#[database("db")]
pub struct Db(diesel::PgConnection);

pub fn setup(rocket: Rocket) -> Rocket {
    let rocket = rocket.attach(Db::fairing());
    let db = Db::get_one(&rocket).unwrap();
    database::setup(&db);
    rocket
}
