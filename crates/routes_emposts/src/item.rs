use database::empost::Empost;
use database::shipment::shipment_extra::LoadWithStatuses;
use database::shipment::{Shipment, ShipmentStatus};
use database::IdType;
use rocket::response::Responder;
use rocket_contrib::templates::Template;
use view_base::app_page::AppPage;
use view_base::session::AdminSession;
use db_provider::Db;

#[derive(Serialize)]
struct Page {
    shipments: Vec<(Shipment, Option<ShipmentStatus>)>,
    empost: Empost,
}

#[derive(Responder)]
enum Response {
    Page(AppPage<Page>),
    Print(Template),
}

#[get("/emposts/<id>?<print>", rank = 3)]
pub(crate) fn get(
    _session: AdminSession,
    db: Db,
    id: IdType,
    print: Option<String>,
) -> impl Responder<'static> {
    let empost = Empost::by_id(id, &db.0)?;

    let shipments = Shipment::by_empost(empost.id, &db).load_last_status(&db);

    let page = Page { shipments, empost };

    if print.is_none() {
        Some(Response::Page(AppPage::with_template("emposts/item", page)))
    } else {
        Some(Response::Print(Template::render("emposts/print", page)))
    }
}
