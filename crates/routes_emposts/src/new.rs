use database::shipment::shipment_extra::SearchParams;
use rocket::response::Redirect;
use rocket::response::Responder;

use database::shipment::status_values::{StatusFilter, STATUS_FILTER_ALL};
use database::shipment::{shipment_extra, Shipment, ShipmentStatus};
use database::IdType;
use logic::empost;
use view_base::app_page::AppPage;
use view_base::session::AdminSession;
use view_base::utils::parse_js_date;
use db_provider::Db;

#[derive(Serialize, Debug)]
pub(crate) struct Page {
    from_date: Option<String>,
    to_date: Option<String>,
    shipments_filters: &'static [StatusFilter],
    shipments_filter: Option<String>,
    shipments: Vec<(Shipment, Option<ShipmentStatus>)>,
    msg: Option<String>,
}

fn search_unemposted(
    search_params: &SearchParams,
    db: &Db,
) -> Vec<(Shipment, Option<ShipmentStatus>)> {
    let filter_fn = |s: &Shipment| s.empost_id.is_none();
    shipment_extra::load_shipments(&search_params, filter_fn, &db)
}

#[get("/emposts/new?<from_date>&<to_date>&<filter>", rank = 1)]
pub(crate) fn get(
    db: Db,
    _session: AdminSession,
    from_date: Option<String>,
    to_date: Option<String>,
    filter: Option<String>,
) -> impl Responder<'static> {
    if from_date.is_none() && to_date.is_none() && filter.is_none() {
        return AppPage::with_template(
            "emposts/new",
            Page {
                from_date: None,
                to_date: None,
                shipments_filters: &STATUS_FILTER_ALL,
                shipments_filter: None,
                shipments: Vec::new(),
                msg: None,
            },
        );
    }
    match SearchParams::try_parse(&from_date, &to_date, &filter, parse_js_date) {
        Ok(search_params) => {
            let shipments = search_unemposted(&search_params, &db);
            let msg = if shipments.is_empty() {
                Some("No shipments found for given search".to_string())
            } else {
                None
            };
            AppPage::with_template(
                "emposts/new",
                Page {
                    from_date,
                    to_date,
                    shipments_filters: &STATUS_FILTER_ALL,
                    shipments_filter: filter,
                    shipments,
                    msg,
                },
            )
        }
        Err(msg) => AppPage::with_template(
            "emposts/new",
            Page {
                from_date,
                to_date,
                shipments_filters: &STATUS_FILTER_ALL,
                shipments_filter: filter,
                shipments: Vec::new(),
                msg: Some(msg),
            },
        ),
    }
}

#[post("/emposts/new?<from_date>&<to_date>&<filter>", rank = 2)]
pub(crate) fn post(
    db: Db,
    _session: AdminSession,
    from_date: Option<String>,
    to_date: Option<String>,
    filter: Option<String>,
) -> Option<Result<Redirect, AppPage<Page>>> {
    if from_date.is_none() && to_date.is_none() && filter.is_none() {
        let msg = Some("Please search for shipments before generating empost".to_string());
        return Some(Err(AppPage::with_template(
            "emposts/new",
            Page {
                from_date: None,
                to_date: None,
                shipments_filters: &STATUS_FILTER_ALL,
                shipments_filter: None,
                shipments: Vec::new(),
                msg,
            },
        )));
    }

    let search_params =
        SearchParams::try_parse(&from_date, &to_date, &filter, parse_js_date).ok()?;
    let shipments = search_unemposted(&search_params, &db);
    let shipments_iter = shipments.iter().map(|(shipment, _status)| shipment);

    let empost = empost::calculate(
        search_params.from_date,
        search_params.to_date,
        shipments_iter,
    );
    let id = empost.save(&db);

    let id_list: Vec<IdType> = shipments
        .iter()
        .map(|(shipment, _status)| shipment.id)
        .collect();
    Shipment::update_empost(&id_list, Some(id), &db);
    Some(Ok(Redirect::to(uri!(super::index::get))))
}
