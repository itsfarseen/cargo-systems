use database::empost::Empost;
use rocket::response::Responder;
use view_base::app_page::AppPage;
use view_base::session::AdminSession;
use db_provider::Db;

#[derive(Serialize)]
struct Page {
    emposts: Vec<Empost>,
}

#[get("/emposts", rank = 0)]
pub(crate) fn get(_session: AdminSession, db: Db) -> impl Responder<'static> {
    let emposts = Empost::all(&db.0);

    AppPage::with_template("emposts/index", Page { emposts })
}
