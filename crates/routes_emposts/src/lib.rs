#![feature(decl_macro)]

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate serde_derive;

use config::Config;
use rocket::Route;

mod delete;
mod index;
mod item;
mod new;

pub fn routes() -> Vec<Route> {
    let config = Config::get();
    if config.empost.is_some() {
        routes![index::get, item::get, delete::post, new::get, new::post]
    } else {
        Vec::new()
    }
}
