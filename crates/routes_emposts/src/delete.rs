use database::empost::Empost;
use database::IdType;
use rocket::response::{Redirect, Responder};
use view_base::session::AdminSession;
use db_provider::Db;

#[post("/emposts/<id>?delete", rank = 4)]
pub(crate) fn post(_session: AdminSession, db: Db, id: IdType) -> impl Responder<'static> {
    let empost = Empost::by_id(id, &db.0)?;

    empost.delete(&db.0);
    Some(Redirect::to(uri!(super::index::get)))
}
