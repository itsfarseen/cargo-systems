use config::Config;
use database::{empost::NewEmpost, shipment::Shipment};

use chrono::{Local, NaiveDate};

pub fn calculate<'a>(
    from_date: NaiveDate,
    to_date: NaiveDate,
    shipments: impl Iterator<Item = &'a Shipment>,
) -> NewEmpost {
    let config = Config::get();

    let empost_cfg = config.empost.as_ref().unwrap();
    let vat_pct = empost_cfg.vat_pct; // config.empost_vat_pct, not same as config.vat_pct
    let empost_pct = empost_cfg.empost_pct;
    let empost_min_delivery_charge = empost_cfg.min_delivery_charge;
    let empost_max_weight = empost_cfg.max_weight;

    let vat_frac = vat_pct / 100.0;
    let empost_frac = empost_pct / 100.0;

    let mut above_weight_num_shipments = 0;
    let mut above_weight_sum_delivery_charge = 0.0;
    let mut below_min_charge_num_shipments = 0;
    let mut other_num_shipments = 0;
    let mut other_sum_delivery_charge = 0.0;

    for shipment in shipments {
        if shipment.weight >= empost_max_weight {
            above_weight_num_shipments += 1;
            above_weight_sum_delivery_charge += shipment.delivery_charge;
        } else if shipment.delivery_charge < empost_min_delivery_charge {
            below_min_charge_num_shipments += 1;
        } else {
            other_num_shipments += 1;
            other_sum_delivery_charge += shipment.delivery_charge;
        }
    }

    let above_weight_empost_amt = 0.0;
    let above_weight_empost_vat = above_weight_empost_amt * vat_frac;

    let below_min_charge_sum_delivery_charge =
        below_min_charge_num_shipments as f64 * empost_min_delivery_charge;
    let below_min_charge_empost_amt = below_min_charge_sum_delivery_charge * empost_frac;
    let below_min_charge_empost_vat = below_min_charge_empost_amt * vat_frac;

    let other_empost_amt = other_sum_delivery_charge * empost_frac;
    let other_empost_vat = other_empost_amt * vat_frac;

    let num_shipments =
        above_weight_num_shipments + below_min_charge_num_shipments + other_num_shipments;

    let sum_delivery_charge = above_weight_sum_delivery_charge
        + below_min_charge_sum_delivery_charge
        + other_sum_delivery_charge;
    let sum_empost_amt = above_weight_empost_amt + below_min_charge_empost_amt + other_empost_amt;
    let sum_empost_vat = sum_empost_amt * vat_frac;

    NewEmpost {
        date_time: Local::now().naive_local(),
        from_date,
        to_date,
        notes: String::new(),
        vat_pct,
        empost_pct,
        empost_min_delivery_charge,
        empost_max_weight,
        above_weight_num_shipments,
        above_weight_sum_delivery_charge,
        above_weight_empost_amt,
        above_weight_empost_vat,
        below_min_charge_num_shipments,
        below_min_charge_sum_delivery_charge,
        below_min_charge_empost_amt,
        below_min_charge_empost_vat,
        other_num_shipments,
        other_sum_delivery_charge,
        other_empost_amt,
        other_empost_vat,
        num_shipments,
        sum_delivery_charge,
        sum_empost_amt,
        sum_empost_vat,
    }
}
