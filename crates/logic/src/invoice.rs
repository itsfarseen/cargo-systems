use chrono::Local;
use config::Config;
use database::invoice::NewInvoice;
use database::shipment::{status_values, Shipment, ShipmentStatus};
use database::IdType;

pub fn calculate(
    client_id: IdType,
    shipments: &[(Shipment, Option<ShipmentStatus>)],
) -> NewInvoice {
    let mut sum_material_cost = 0.0;
    let mut num_deliveries = 0;
    let mut num_returns = 0;
    let mut num_in_transit = 0;
    let mut sum_delivery_charge = 0f64;
    let mut sum_return_charge = 0f64;

    for (shipment, status) in shipments {
        if let Some(status) = status {
            match status.status.as_str() {
                status_values::DELIVERED => {
                    num_deliveries += 1;
                    sum_material_cost += shipment.material_cost;
                    sum_delivery_charge += shipment.delivery_charge;
                }
                status_values::RETURNED_TO_CLIENT | status_values::RETURNED_TO_OFFICE => {
                    num_returns += 1;
                    sum_return_charge += shipment.return_charge;
                }
                _ => num_in_transit += 1,
            }
        } else {
            num_in_transit += 1
        }
    }

    let config = Config::get();

    let vat_pct = config.vat_pct;

    let vat_frac = 1.0 + vat_pct / 100.0;

    let vat_incl_sum_delivery_charge = vat_frac * sum_delivery_charge;
    let vat_incl_sum_return_charge = vat_frac * sum_return_charge;

    let num_shipments = num_returns + num_deliveries + num_in_transit;
    let invoice_amt = vat_incl_sum_delivery_charge + vat_incl_sum_return_charge;
    let net_payable = sum_material_cost - invoice_amt;

    NewInvoice {
        client_id,
        date_time: Local::now().naive_local(),
        notes: String::new(),
        sum_material_cost,
        vat_pct,
        num_deliveries,
        sum_delivery_charge,
        vat_incl_sum_delivery_charge,
        num_returns,
        sum_return_charge,
        vat_incl_sum_return_charge,
        num_in_transit,
        num_shipments,
        invoice_amt,
        net_payable,
    }
}
