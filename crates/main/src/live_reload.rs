use hotwatch::{
    blocking::{Flow, Hotwatch},
    Event,
};

use std::cell::RefCell;
use std::net::TcpListener;
use std::{thread::spawn, time::Duration};
use tungstenite::server::accept;

pub fn watch() {
    let server = TcpListener::bind("0.0.0.0:8001").unwrap();
    for stream in server.incoming() {
        spawn(move || {
            let stream = stream.unwrap();
            let websocket = RefCell::from(accept(stream).unwrap());
            let mut hotwatch = Hotwatch::new_with_custom_delay(Duration::from_millis(10))
                .expect("hotwatch failed to initialize!");
            hotwatch
                .watch(".", move |event: Event| {
                    if let Some(path) = match event {
                        Event::Create(path) => Some(path),
                        Event::Write(path) => Some(path),
                        _ => None,
                    } {
                        if path.to_str().unwrap().contains("target/debug") {
                            return Flow::Continue;
                        }
                        let msg = if path.to_str().unwrap().ends_with(".rs") {
                            "reload_delayed".to_owned()
                        } else {
                            "reload".to_owned()
                        };

                        websocket
                            .borrow_mut()
                            .write_message(tungstenite::Message::Text(msg))
                            .unwrap();
                        Flow::Exit
                    } else {
                        Flow::Continue
                    }
                })
                .expect("failed to watch files for live_reload!");
            hotwatch.run();
        });
    }
}
