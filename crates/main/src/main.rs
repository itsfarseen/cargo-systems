// #![allow(unused_imports, dead_code, unused_variables, unused_mut)]
#![feature(never_type)] // For error type in in-memory db
#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use]
extern crate rocket;

extern crate dotenv;
use db_provider::Db;
use rocket::response::status::NotFound;
use rocket::response::{NamedFile, Redirect, Responder};

use once_cell::sync::OnceCell;
use std::path::{Path, PathBuf};

use view_base::session::UserSession;
mod live_reload;

static ENV_TEST: OnceCell<bool> = OnceCell::new();
static LIVE_RELOAD: OnceCell<bool> = OnceCell::new();

fn main() {
    let env_test = std::env::var("ENV_TEST")
        .map(|x| x.to_lowercase().eq("true"))
        .unwrap_or(false);
    ENV_TEST.set(env_test).unwrap();

    let live_reload = std::env::var("LIVE_RELOAD")
        .map(|x| x.to_lowercase().eq("true"))
        .unwrap_or(false);

    LIVE_RELOAD.set(live_reload).unwrap();

    if *LIVE_RELOAD.get().unwrap() {
        std::thread::spawn(live_reload::watch);
    }

    let dotenv_loaded = if *ENV_TEST.get().unwrap() {
        dotenv::from_filename(".env.test")
    } else {
        dotenv::dotenv()
    };

    if dotenv_loaded.is_err() {
        println!("INFO: Dotenv not loaded");
    } else {
        println!("INFO: Dotenv loaded");
    }

    config::Config::load();

    let rocket = rocket::ignite();
    let rocket = view_base::setup(rocket);
    let rocket = db_provider::setup(rocket);

    let rocket = routes_invoices::setup(rocket);
    rocket
        .mount("/", routes_awb::routes())
        .mount("/", routes_dashboard::routes())
        .mount("/", routes_emposts::routes())
        .mount("/", routes_invoices::routes())
        .mount("/", routes_login::routes())
        .mount("/", routes_shipments::routes())
        .mount("/", routes_users::routes())
        .mount("/", routes_reports::routes())
        .mount("/", routes![reset_db, static_files, catch_all])
        .launch();
}

#[get("/<_path..>", rank = 999)]
fn catch_all(session: Option<UserSession>, _path: PathBuf) -> impl Responder<'static> {
    if let Some(_session) = session {
        Redirect::to("/")
    } else {
        Redirect::to("/login")
    }
}

#[get("/static/<file..>")]
fn static_files(file: PathBuf) -> Result<NamedFile, NotFound<String>> {
    if file.eq(&PathBuf::from("live_reload.js")) && !LIVE_RELOAD.get().unwrap() {
        return Err(NotFound(file.to_string_lossy().into()));
    }

    let path = Path::new("static/").join(file);
    NamedFile::open(&path).map_err(|e| NotFound(e.to_string()))
}

#[post("/testing/reset_db")]
fn reset_db(db: Db) -> impl Responder<'static> {
    if !*ENV_TEST.get().unwrap() {
        return None;
    }

    database::reset_db(&db);

    return Some("");
}
