use chrono::NaiveDate;
use rocket::response::Responder;
use rocket::Route;
use rocket_contrib::templates::Template;

use database::shipment::Shipment;
use database::user::UserRole;
use database::IdType;
use view_base::session::UserSession;
use db_provider::Db;

pub fn routes() -> Vec<Route> {
    routes![awb_multiple, awb_single]
}

#[derive(Serialize)]
struct AWB<'a> {
    shipments: &'a [Shipment],
}

#[get("/awb?<from_date>&<to_date>&<client>", rank = 2)]
fn awb_multiple(
    session: UserSession,
    db: Db,
    from_date: Option<String>,
    to_date: Option<String>,
    client: Option<IdType>,
) -> impl Responder<'static> {
    let client_id = {
        match session.user_role {
            UserRole::Admin => client?,
            UserRole::Client => {
                if client.is_some() {
                    return None;
                } else {
                    session.id
                }
            }
            _ => return None,
        }
    };
    let parsed_from_date = from_date
        .as_ref()
        .and_then(|d| NaiveDate::parse_from_str(d, "%Y-%m-%d").ok());
    let parsed_to_date = to_date
        .as_ref()
        .and_then(|d| NaiveDate::parse_from_str(d, "%Y-%m-%d").ok());

    let (parsed_from_date, parsed_to_date) = match (parsed_from_date, parsed_to_date) {
        (Some(x), Some(y)) => (x, y),
        _ => {
            return Some(Err(rocket::response::status::BadRequest(Some(
                "Please specify both From and To dates.",
            ))))
        }
    };

    let shipments =
        Shipment::by_client_and_date_range(client_id, (parsed_from_date, parsed_to_date), &db.0);

    Some(Ok(Template::render(
        "print_awb",
        AWB {
            shipments: &shipments,
        },
    )))
}

#[get("/awb?<shipment>", rank = 1)]
fn awb_single(session: UserSession, db: Db, shipment: IdType) -> impl Responder<'static> {
    let shipment = Shipment::by_id(shipment, &db.0)?;
    if session.user_role != UserRole::Admin && shipment.client_id != session.id {
        return None;
    }
    Some(Template::render(
        "print_awb",
        AWB {
            shipments: &[shipment],
        },
    ))
}
