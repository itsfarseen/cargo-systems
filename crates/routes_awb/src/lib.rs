#![feature(decl_macro)]

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate serde_derive;

use rocket::Route;

mod awb;

pub fn routes() -> Vec<Route> {
    let mut routes = Vec::new();
    routes.append(&mut awb::routes());
    routes
}
