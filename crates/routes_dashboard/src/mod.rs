use rocket::{
    response::{Redirect, Responder},
    Route,
};

mod admin;
mod client;
mod driver;

#[get("/", rank = 9)]
fn default_dashboard() -> impl Responder<'static> {
    Redirect::to("/shipments/track")
}

pub fn routes() -> Vec<Route> {
    let mut routes = admin::routes();
    routes.extend(client::routes());
    routes.extend(driver::routes());
    routes.extend(routes![default_dashboard]);
    routes
}
