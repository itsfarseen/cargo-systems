#![feature(decl_macro)]

#[macro_use]
extern crate rocket;

use rocket::{
    response::{Redirect, Responder},
    Route,
};

mod admin;
mod client;
mod driver;

#[get("/", rank = 9)]
fn default_dashboard() -> impl Responder<'static> {
    Redirect::to("/shipments/track")
}

pub fn routes() -> Vec<Route> {
    let mut routes = routes![default_dashboard];
    routes.append(&mut admin::routes());
    routes.append(&mut client::routes());
    routes.append(&mut driver::routes());
    routes
}
