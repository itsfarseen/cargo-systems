use chrono::Local;
use chrono_utilities::naive::DateTransitions;
use database::shipment::shipment_extra::LoadWithStatuses;
use database::shipment::{status_values, Shipment, ShipmentStatus};
use db_provider::Db;
use rocket::{response::Responder, Route};
use serde_derive::Serialize;
use view_base::app_page::AppPage;
use view_base::session::AdminSession;

pub fn routes() -> Vec<Route> {
    routes![dashboard]
}

#[derive(Serialize)]
struct DashboardPage {
    month_start: String,
    month_end: String,
    today: String,
    monthly_booked: i32,
    monthly_delivered: i32,
    monthly_delivery_pct: f64,
    daily_booked: i32,
    daily_total_material_cost: f64,
}

#[get("/", rank = 2)]
fn dashboard(_session: AdminSession, db: Db) -> impl Responder<'static> {
    let today = Local::now().naive_local().date();
    let month_start = today.start_of_month().unwrap();
    let month_end = today.end_of_month().unwrap();
    let monthly_shipments: Vec<(Shipment, Option<ShipmentStatus>)> =
        Shipment::by_date_range((month_start, month_end), &db).load_last_status(&db);

    let monthly_booked = monthly_shipments.len() as i32;
    let mut monthly_delivered = 0;
    let mut daily_booked = 0;
    let mut daily_total_material_cost = 0.0;
    for (shipment, status) in &monthly_shipments {
        let is_today = shipment.date_time.date() == today;
        if is_today {
            daily_booked += 1;
            daily_total_material_cost += shipment.material_cost;
        }
        match status {
            Some(status) if status.status.as_str() == status_values::DELIVERED => {
                monthly_delivered += 1;
            }
            _ => {}
        }
    }

    let monthly_delivery_pct = if monthly_booked > 0 {
        (monthly_delivered as f64 / monthly_booked as f64) * 100.0
    } else {
        0.0
    };

    let page = DashboardPage {
        month_start: month_start.format("%d/%m/%Y").to_string(),
        month_end: month_end.format("%d/%m/%Y").to_string(),
        today: today.format("%d/%m/%Y").to_string(),
        monthly_booked,
        monthly_delivered,
        monthly_delivery_pct,
        daily_booked,
        daily_total_material_cost,
    };
    AppPage::with_template("dashboard_admin", page)
}
