use rocket::{response::Redirect, response::Responder, Route};
use view_base::session::DriverSession;

pub fn routes() -> Vec<Route> {
    routes![dashboard]
}

#[get("/", rank = 3)]
pub fn dashboard(_session: DriverSession) -> impl Responder<'static> {
    Redirect::to("/shipments/scan")
}
