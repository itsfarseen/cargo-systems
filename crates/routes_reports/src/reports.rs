use chrono::NaiveDate;
use database::shipment::shipment_extra::LoadWithStatuses;
use database::shipment::{status_values, Shipment, ShipmentStatus};
use database::user::{User, UserRole};
use database::IdType;
use db_provider::Db;
use rocket::{
    http::ContentType, request::FromParam, response::Content, response::Responder, Route,
};
use rocket_contrib::templates::Template;
use view_base::{app_page::AppPage, session::UserSession};

pub fn routes() -> Vec<Route> {
    routes![reports,]
}

enum ReportFormat {
    Pdf,
    Csv,
}

impl<'a> FromParam<'a> for ReportFormat {
    type Error = ();

    fn from_param(param: &'a rocket::http::RawStr) -> Result<Self, Self::Error> {
        match param.as_str() {
            "pdf" => Ok(ReportFormat::Pdf),
            "csv" => Ok(ReportFormat::Csv),
            _ => Err(()),
        }
    }
}
#[derive(Serialize)]
struct ReportsPage {
    client_name: String,
    client_id: Option<IdType>,
    shipments: Vec<(Shipment, Option<ShipmentStatus>)>,
    sum_material_cost: f64,
    sum_delivery_charge: f64,
    sum_return_charge: f64,
    from_date: String,
    to_date: String,
}

#[derive(Responder)]
enum ReportsResponse {
    Page(AppPage<ReportsPage>),
    Print(Template),
    CSV(Content<String>),
}

#[get("/reports?<client>&<from_date>&<to_date>&<pdf>&<csv>")]
fn reports(
    session: UserSession,
    db: Db,
    client: Option<IdType>,
    from_date: Option<String>,
    to_date: Option<String>,
    pdf: Option<String>,
    csv: Option<String>,
) -> impl Responder<'static> {
    let client_id = {
        match session.user_role {
            UserRole::Admin => client?,
            UserRole::Client => {
                if client.is_some() {
                    return None;
                } else {
                    session.id
                }
            }
            _ => return None,
        }
    };

    let user = User::by_id(client_id, &db.0)?;
    let client = {
        // Only show reports for a Client or Admin role.
        if user.user_role == UserRole::Client || user.user_role == UserRole::Admin {
            user
        } else {
            return None;
        }
    };

    let from_date_parsed = from_date
        .as_ref()
        .and_then(|d| NaiveDate::parse_from_str(d, "%Y-%m-%d").ok());
    let to_date_parsed = to_date
        .as_ref()
        .and_then(|d| NaiveDate::parse_from_str(d, "%Y-%m-%d").ok());

    let mut shipments = Vec::new();
    if from_date_parsed.is_some() && to_date_parsed.is_some() {
        shipments = Shipment::by_client_and_date_range(
            client_id,
            (from_date_parsed.unwrap(), to_date_parsed.unwrap()),
            &db.0,
        )
        .load_last_status(&db);
    }

    let mut sum_material_cost = 0.0;
    let mut sum_delivery_charge = 0.0;
    let mut sum_return_charge = 0.0;
    for (shipment, status) in &shipments {
        sum_material_cost += shipment.material_cost;
        match status {
            Some(status) => {
                if status_values::DELIVERED_ALL.contains(&status.status.as_str()) {
                    sum_delivery_charge += shipment.delivery_charge;
                } else if status_values::RETURNED_ALL.contains(&status.status.as_str()) {
                    sum_return_charge += shipment.return_charge;
                }
            }

            None => {}
        }
    }

    match (pdf, csv, shipments.is_empty()) {
        (None, None, _) | (_, _, true) => {
            let page = ReportsPage {
                client_name: client.name,
                client_id: if session.user_role == UserRole::Admin {
                    Some(client.id)
                } else {
                    None
                },
                from_date: from_date.unwrap_or_else(|| "".to_owned()),
                to_date: to_date.unwrap_or_else(|| "".to_owned()),
                shipments,
                sum_material_cost,
                sum_delivery_charge,
                sum_return_charge,
            };
            Some(ReportsResponse::Page(AppPage::with_template(
                "reports", page,
            )))
        }
        (Some(_), _, false) => {
            let (from_date, to_date) = (from_date?, to_date?);
            let page = ReportsPage {
                client_name: client.name,
                client_id: if session.user_role == UserRole::Admin {
                    Some(client.id)
                } else {
                    None
                },
                from_date,
                to_date,
                shipments,
                sum_material_cost,
                sum_delivery_charge,
                sum_return_charge,
            };
            Some(ReportsResponse::Print(Template::render(
                "print_reports",
                page,
            )))
        }
        (_, Some(_), false) => {
            let mut csv = String::new();
            csv.push_str("Sl., AWB #, Your Ref, Receiver, City, Area, Wt., Pc., Status\n");
            for (i, (shipment, status)) in shipments.iter().enumerate() {
                csv.push_str(&format!(
                    "\"{}\", \"{}\", \"{}\",\"{}\", \"{}\", \"{}\", \"{}\", \"{}\", \"{}\"\n",
                    i + 1,
                    shipment.id,
                    shipment.your_ref,
                    shipment.receiver_name,
                    shipment.receiver_city,
                    shipment.receiver_area,
                    shipment.weight,
                    shipment.pieces,
                    status
                        .as_ref()
                        .map(|x| &x.status)
                        .unwrap_or(&"N/A".to_owned())
                ));
            }
            Some(ReportsResponse::CSV(Content(ContentType::CSV, csv)))
        }
    }
}
