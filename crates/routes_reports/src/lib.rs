#![feature(decl_macro)]

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate serde_derive;

use rocket::Route;

mod reports;

pub fn routes() -> Vec<Route> {
    let mut routes = Vec::new();
    routes.append(&mut reports::routes());
    routes
}
