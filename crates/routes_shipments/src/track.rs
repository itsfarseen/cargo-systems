use std::collections::HashMap;

use database::shipment::{Shipment, ShipmentStatus};
use database::user::{GetUserIds, User};
use database::IdType;
use if_chain::if_chain;
use rocket::{request::Form, response::Redirect, response::Responder, Route};
use serde_derive::Serialize;
use view_base::app_page::AppPage;
use view_base::session::{AdminSession, ClientSession};
use db_provider::Db;

pub fn routes() -> Vec<Route> {
    routes![shipments_item, track, track_post]
}

#[get("/shipments/track")]
fn track(session: Option<ClientSession>) -> impl Responder<'static> {
    let page = TrackPage {
        awb: "".to_owned(),
        error_message: "".to_owned(),
        show_your_ref: session.is_some(),
        your_ref: "".to_owned(),
    };
    AppPage::with_template("shipments/track", page)
}

#[derive(FromForm)]
struct TrackForm {
    awb: String,
    your_ref: Option<String>,
}

#[derive(Serialize, Debug)]
struct TrackPage {
    awb: String,
    error_message: String,
    show_your_ref: bool,
    your_ref: String,
}

#[post("/shipments/track", data = "<form>")]
fn track_post(
    session: Option<ClientSession>,
    db: Db,
    form: Form<TrackForm>,
) -> impl Responder<'static> {
    let mut error_message = None;
    if_chain! {
    if let Some(client) = &session;
    if let Some(your_ref) = &form.your_ref;
    if !your_ref.is_empty();
    then {
       match Shipment::by_ref(client.id, &your_ref, &db.0) {
            Some(shipment) => {return Ok(Redirect::to(uri!(shipments_item: shipment.id)));}
            None => {error_message = Some("No shipment found with given reference.");}
       }
    }
    };

    let error_message = if let Some(error_message) = error_message {
        error_message
    } else if let Ok(id) = form.awb.parse() {
        if let Some(shipment) = Shipment::by_id(id, &db.0) {
            return Ok(Redirect::to(uri!(shipments_item: shipment.id)));
        } else {
            "No shipment found with given AWB number."
        }
    } else {
        "Invalid AWB number."
    };

    let page = TrackPage {
        awb: form.awb.clone(),
        error_message: error_message.to_owned(),
        show_your_ref: session.is_some(),
        your_ref: form.your_ref.as_ref().unwrap_or(&"".to_owned()).clone(),
    };
    Err(AppPage::with_template("shipments/track", page))
}

#[derive(Serialize)]
struct ShipmentPage {
    shipment: Shipment,
    status: Vec<ShipmentStatus>,
    client: User,
    show_privileged_info: bool,
    updated_users_map: Option<HashMap<IdType, User>>,
}

#[get("/shipments/<id>")]
fn shipments_item(
    admin_session: Option<AdminSession>,
    db: Db,
    id: IdType,
) -> impl Responder<'static> {
    let shipment = Shipment::by_id(id, &db.0)?;
    let status = ShipmentStatus::by_shipment_all_status(&shipment, &db.0);
    let updated_user_ids = status.get_user_ids();
    let updated_user_ids: Vec<i32> = updated_user_ids.into_iter().collect();
    let users = User::by_ids(&updated_user_ids, &db.0);

    let users_hashmap = if admin_session.is_some() {
        let mut users_hashmap = HashMap::new();
        for user in users {
            users_hashmap.insert(user.id, user);
        }
        Some(users_hashmap)
    } else {
        None
    };

    let client = User::by_id(shipment.client_id, &db.0)?;
    Some(AppPage::with_template(
        "shipments/item",
        ShipmentPage {
            show_privileged_info: admin_session.is_some(),
            shipment,
            status,
            client,
            updated_users_map: users_hashmap,
        },
    ))
}
