use calamine::Reader;
use database::{
    shipment::{status_values, NewShipment, NewShipmentStatus},
    user::ClientInfo,
};
use multipart::server::Multipart;
use rocket::response::{status::Custom, Redirect};
use rocket::{
    http::{ContentType, Status},
    Route,
};
use std::io::{Cursor, Read};
use view_base::session::ClientSession;
use db_provider::Db;

pub fn routes() -> Vec<Route> {
    routes![shipments_upload]
}

#[post("/shipments/upload", data = "<data>")]
fn shipments_upload(
    session: ClientSession,
    db: Db,
    cont_type: &ContentType,
    data: rocket::Data,
) -> Result<Redirect, Custom<String>> {
    // this and the next check can be implemented as a request guard but it seems like just
    // more boilerplate than necessary
    if !cont_type.is_form_data() {
        return Err(Custom(
            Status::BadRequest,
            "Content-Type not multipart/form-data".into(),
        ));
    }

    let (_, boundary) = cont_type
        .params()
        .find(|&(k, _)| k == "boundary")
        .ok_or_else(|| {
            Custom(
                Status::BadRequest,
                "`Content-Type: multipart/form-data` boundary param not provided".into(),
            )
        })?;
    let mut multipart = Multipart::with_body(data.open(), boundary);
    let field_entry = multipart.read_entry();
    let mut field = field_entry.unwrap().unwrap();
    let mut data = Vec::new();
    field.data.read_to_end(&mut data).unwrap();
    let cursor = Cursor::new(data);

    let mut awb_file = calamine::Xlsx::new(cursor).unwrap();
    let sheet = awb_file.worksheet_range_at(0).unwrap().unwrap();

    // check fields
    let fields = [
        "Your Ref #",
        "Contact Name",
        "Contact Number",
        "Address",
        "City",
        "Area",
        "Weight",
        "Pieces",
        "Material Cost",
        "SPL Instruction",
        "Remarks",
        "Your Company",
    ];

    let mut rows = sheet.rows();

    let head_row = rows.next();

    if head_row.is_none() {
        return Err(Custom(
            Status::BadRequest,
            "Given excel file is empty.\n".into(),
        ));
    }
    let head_row = head_row.unwrap();

    // Validate fields
    for (i, &field) in fields.iter().enumerate() {
        let got_field = head_row.get(i);
        match got_field {
            Some(got_field) => {
                if got_field.to_string().as_str() != field {
                    return Err(Custom(
                        Status::BadRequest,
                        format!(
                            "Given excel file is not of the expected format.\n\
                             Expected {} at column {}, but got {}.\n\
                             Make sure you follow the reference excel file",
                            field,
                            i + 1,
                            got_field
                        ),
                    ));
                }
            }
            None => {
                return Err(Custom(
                    Status::BadRequest,
                    format!(
                        "Given excel file is not of the expected format.\n\
                         Expected {} at column {}, but that column is not found in uploaded file.\n\
                         Please make sure you follow the reference excel file",
                        field,
                        i + 1
                    ),
                ));
            }
        }
    }

    let mut shipments = Vec::new();

    for (i, row) in rows.enumerate() {
        if row.len() != fields.len() {
            return Err(Custom(
                Status::BadRequest,
                format!(
                    "Given excel file is not of the expected format.\n\
                         Row {} has invalid length. Expected length {}, got length {}.
                         Please make sure you follow the reference excel file",
                    i + 2,
                    fields.len(),
                    row.len()
                ),
            ));
        }

        let client = session.clone();
        let client_info = ClientInfo::by_id(client.id, &db.0).unwrap();

        let your_ref = row[0].to_string().to_uppercase();
        let receiver_name = row[1].to_string();
        let receiver_phone = row[2].to_string();
        let receiver_addr = row[3].to_string();
        let receiver_city = row[4].to_string();
        let receiver_area = row[5].to_string();
        let weight = get_float(&row[6]).unwrap();
        let pieces = get_usize(&row[7]).unwrap();
        let material_cost = get_float(&row[8]).unwrap();
        let spl_instrs = row[9].to_string();
        let remarks = row[10].to_string();
        let your_company = row[11].to_string();
        let invoice_id = None;
        let sender_name = client.name;
        let sender_addr = client.address;
        let sender_phone = client.phone;
        let delivery_charge = client_info.delivery_charge;
        let return_charge = client_info.return_charge;

        shipments.push(NewShipment {
            client_id: client.id,
            date_time: chrono::Local::now().naive_local(),
            your_ref,
            receiver_name,
            receiver_phone,
            receiver_addr,
            receiver_city,
            receiver_area,
            weight,
            pieces: pieces as i32,
            material_cost,
            spl_instrs,
            remarks,
            your_company,
            invoice_id,
            sender_name,
            sender_addr,
            sender_phone,
            delivery_charge,
            return_charge,
        });
    }
    let ids = NewShipment::save_many(&shipments, &db.0);

    let statuses: Vec<NewShipmentStatus> = ids
        .iter()
        .map(|id| NewShipmentStatus {
            shipment_id: *id,
            user_id: session.id,
            date_time: chrono::Local::now().naive_local(),
            location: "Client Office".to_owned(),
            status: status_values::BOOKED.to_owned(),
            remarks: "".to_owned(),
        })
        .collect();
    NewShipmentStatus::save_many(&statuses, &db.0);

    Ok(Redirect::to("/"))
}

fn get_float(data: &calamine::DataType) -> Option<f64> {
    match data {
        calamine::DataType::Int(i) => Some(*i as _),
        calamine::DataType::Float(f) => Some(*f),
        calamine::DataType::String(s) => s.parse().ok(),
        _ => None,
    }
}

fn get_usize(data: &calamine::DataType) -> Option<usize> {
    match data {
        calamine::DataType::Int(i) => Some(*i as _),
        calamine::DataType::Float(f) => Some(*f as _),
        calamine::DataType::String(s) => s.parse().ok(),
        _ => None,
    }
}
