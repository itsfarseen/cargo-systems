use database::shipment::{status_values, NewShipmentStatus, Shipment, ShipmentStatus};
use database::user::UserRole;
use database::IdType;
use db_provider::Db;
use rocket::{
    http::{Cookie, Cookies},
    request::{FlashMessage, Form},
    response::Responder,
    response::{Flash, Redirect},
    Route,
};
use serde_derive::Serialize;
use view_base::app_page::AppPage;
use view_base::session::*;

pub fn routes() -> Vec<Route> {
    routes![
        shipments_scan,
        shipments_scan_add,
        shipments_scan_reset,
        shipments_scan_remove,
        shipments_status_update
    ]
}

#[derive(Default, Serialize)]
struct ShipmentsScanPage {
    scanned_items: Vec<(Shipment, Option<ShipmentStatus>)>,
    message: String,
    update_status_options: &'static [&'static str],
}

#[get("/shipments/scan")]
fn shipments_scan(
    session: UserSession,
    mut db: Db,
    flash: Option<FlashMessage>,
    mut cookies: Cookies,
) -> impl Responder<'static> {
    if ![UserRole::Admin, UserRole::Driver].contains(&session.user_role) {
        return None;
    }

    let (_, scanned_items) = shipments_scanned_items(&mut db, &mut cookies);
    std::mem::drop(cookies);
    let page = ShipmentsScanPage {
        update_status_options: status_values::ALL,
        message: if let Some(message) = flash {
            message.msg().to_owned()
        } else {
            String::new()
        },
        scanned_items,
    };
    Some(AppPage::with_template("shipments/scan", page))
}

#[post("/shipments/scan/reset")]
fn shipments_scan_reset(session: UserSession, mut cookies: Cookies) -> impl Responder<'static> {
    if ![UserRole::Admin, UserRole::Driver].contains(&session.user_role) {
        return None;
    }

    let mut cookie = Cookie::named(SHIPMENT_IDS_COOKIE);
    cookie.set_path("/");
    cookies.remove(cookie);
    Some(Flash::success(
        Redirect::to(uri!(shipments_scan)),
        "List cleared",
    ))
}

#[derive(FromForm)]
struct ShipmentsScanRemoveForm {
    item_id: String,
}

#[post("/shipments/scan/remove", data = "<data>")]
fn shipments_scan_remove(
    data: Form<ShipmentsScanRemoveForm>,
    session: UserSession,
    mut cookies: Cookies,
) -> impl Responder<'static> {
    if ![UserRole::Admin, UserRole::Driver].contains(&session.user_role) {
        return None;
    }

    let mut item_ids = get_item_ids(&cookies);
    let item_id: IdType = {
        if let Ok(item_id) = data.item_id.parse() {
            item_id
        } else {
            return Some(Flash::success(
                Redirect::to(uri!(shipments_scan)),
                "Invalid item",
            ));
        }
    };
    item_ids.retain(|&x| x != item_id);
    set_item_ids(&mut cookies, &item_ids);
    return Some(Flash::success(
        Redirect::to(uri!(shipments_scan)),
        "Item removed",
    ));
}

#[derive(FromForm)]
struct ShipmentsScanAddForm {
    new_item_id: String,
}

#[post("/shipments/scan", data = "<data>")]
fn shipments_scan_add(
    session: UserSession,
    db: Db,
    data: Form<ShipmentsScanAddForm>,
    mut cookies: Cookies,
) -> impl Responder<'static> {
    if ![UserRole::Admin, UserRole::Driver].contains(&session.user_role) {
        return None;
    }

    let (mut item_ids, mut scanned_items) = shipments_scanned_items(&db, &mut cookies);

    let message;
    if scanned_items.len() >= 200 {
        message = "Maximum scan limit reached (200 items). Please update this batch and scan in a new batch.".to_owned();
    } else if let Ok(id) = data.new_item_id.parse::<IdType>() {
        if item_ids.contains(&id) {
            message = "Item already in the list".to_owned();
        } else if let Some(shipment) = Shipment::by_id(id, &db.0) {
            let status = ShipmentStatus::by_shipment_last_status(&shipment, &db.0);
            item_ids.push(id);
            scanned_items.push((shipment, status));
            message = "Item added succesfully.".to_string()
        } else {
            message = "Item not found in the database".to_owned();
        }
    } else {
        message = "Invalid AWB number".to_owned();
    }
    set_item_ids(&mut cookies, &item_ids);

    Some(Flash::new(
        Redirect::to(uri!(shipments_scan)),
        "message",
        message,
    ))
}

#[derive(FromForm)]
struct BulkUpdateShipmentsForm {
    location: String,
    status: String,
    remarks: String,
}

#[post("/shipments/status_update", data = "<data>")]
fn shipments_status_update(
    session: UserSession,
    mut db: Db,
    mut cookies: Cookies,
    data: Form<BulkUpdateShipmentsForm>,
) -> impl Responder<'static> {
    if ![UserRole::Admin, UserRole::Driver].contains(&session.user_role) {
        return None;
    }

    let (_, scanned_items) = shipments_scanned_items(&mut db, &mut cookies);

    let mut status_updates = Vec::new();
    for item in scanned_items {
        status_updates.push(NewShipmentStatus {
            shipment_id: item.0.id,
            date_time: chrono::Local::now().naive_local(),
            location: data.location.clone(),
            status: data.status.clone(),
            remarks: data.remarks.clone(),
            user_id: session.id,
        });
    }
    NewShipmentStatus::save_many(&status_updates, &db.0);

    Some(Some(Flash::success(
        Redirect::to(uri!(shipments_scan)),
        "Status updated successfully",
    )))
}

const SHIPMENT_IDS_COOKIE: &str = "shipments_scan_items";

fn get_item_ids(cookies: &Cookies) -> Vec<IdType> {
    let item_ids_cookie = cookies
        .get(SHIPMENT_IDS_COOKIE)
        .cloned()
        .unwrap_or_else(|| Cookie::new(SHIPMENT_IDS_COOKIE, ""));

    item_ids_cookie
        .value()
        .split(',')
        .map(|x| x.parse::<IdType>())
        .filter_map(Result::ok)
        .collect()
}

fn set_item_ids(cookies: &mut Cookies, item_ids: &[IdType]) {
    let item_ids_str = item_ids
        .iter()
        .map(|i| i.to_string())
        .collect::<Vec<String>>()
        .join(",");
    let mut cookie = Cookie::new(SHIPMENT_IDS_COOKIE, item_ids_str);
    cookie.set_path("/");
    cookies.add(cookie);
}

fn shipments_scanned_items(
    db: &Db,
    cookies: &mut Cookies,
) -> (Vec<IdType>, Vec<(Shipment, Option<ShipmentStatus>)>) {
    let item_ids = get_item_ids(cookies);
    let mut scanned_items: Vec<(Shipment, Option<ShipmentStatus>)> = Vec::new();
    let mut item_ids_checked = Vec::new();
    for id in item_ids {
        if item_ids_checked.contains(&id) {
            continue;
        }
        if let Some(shipment) = Shipment::by_id(id, &db.0) {
            let status = ShipmentStatus::by_shipment_last_status(&shipment, &db.0);
            scanned_items.push((shipment, status));
            item_ids_checked.push(id);
        }
    }
    set_item_ids(cookies, &item_ids_checked);
    (item_ids_checked, scanned_items)
}
