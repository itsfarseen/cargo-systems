use chrono::Local;
use rocket::{request::Form, response::Responder};
use rocket::{response::Redirect, Route};

use database::shipment::{status_values, NewShipment, NewShipmentStatus, Shipment};
use database::user::{ClientInfo, User, UserRole};
use db_provider::Db;
use view_base::app_page::AppPage;
use view_base::session::UserSession;

pub fn routes() -> Vec<Route> {
    routes![page, post]
}

#[derive(Default, Serialize)]
struct Page {
    form: FormFields,
    errors: FormFields,
}

#[get("/shipments/book")]
fn page() -> impl Responder<'static> {
    AppPage::with_template("shipments/book", Page::default())
}

#[derive(FromForm, Serialize, Default)]
struct FormFields {
    pub action_btn: String,
    // search sender
    pub search_sender_awb: String,
    pub search_sender_client_username: String,
    pub search_sender_phone: String,
    // search reciever
    pub search_receiver_awb: String,
    pub search_receiver_phone: String,
    // shipment
    pub client_id: String,
    pub client_username: String,
    pub sender_name: String,
    pub sender_phone: String,
    pub sender_addr: String,
    pub delivery_charge: String,
    pub return_charge: String,
    pub receiver_name: String,
    pub receiver_phone: String,
    pub receiver_addr: String,
    pub receiver_city: String,
    pub receiver_area: String,
    pub material_cost: String,
    pub weight: String,
    pub pieces: String,
    pub spl_instrs: String,
    pub remarks: String,
    pub your_ref: String,
    pub your_company: String,
}

#[derive(Responder)]
enum Response {
    Page(AppPage<Page>),
    Redirect(Redirect),
}

#[post("/shipments/book", data = "<data>")]
fn post(data: Form<FormFields>, session: UserSession, db: Db) -> Option<Response> {
    match data.action_btn.as_str() {
        "search_sender" => Some(Response::Page(search_sender(data, db))),
        "clear_sender" => Some(Response::Page(clear_sender(data, db))),
        "search_receiver" => Some(Response::Page(search_receiver(data, db))),
        "book" => match book(data, session, db) {
            Ok(x) => Some(Response::Redirect(x)),
            Err(Some(x)) => Some(Response::Page(x)),
            Err(None) => None,
        },
        x => panic!("Invalid action_btn '{}'", x),
    }
}

macro_rules! validate {
    ($form: ident, $ok_type: ident, $error_type: ident, { $($ident1: ident: $val1: expr),* }, $(($ident: ident, $expr: expr)),*) => {
        {
            let mut validated = true;
            let mut errors = $error_type::default();
            $(let mut $ident = None;)*
            $(
                let res = $expr(&$form.$ident);
                match res {
                    Ok(val) => {
                        $ident = Some(val)
                    },
                    Err(msg) => {
                        let msg: &str = msg;
                        validated = false;
                        errors.$ident = msg.to_owned();
                    }
                }
            )*
            if validated {
                Ok($ok_type {
                    $($ident1: $val1),*,
                    $($ident: $ident.unwrap()),*
                })
            } else {
                Err(errors)
            }
        }
    };
}

fn book(
    data: Form<FormFields>,
    session: UserSession,
    db: Db,
) -> Result<Redirect, Option<AppPage<Page>>> {
    let form = data.0;

    let shipment_client_id = match session.user_role {
        UserRole::Client => session.id,
        UserRole::Admin => {
            if form.client_id.is_empty() {
                session.id
            } else {
                match form.client_id.parse() {
                    Ok(x) => match User::by_id(x, &db) {
                        Some(user) => user.id,
                        None => return Err(None),
                    },
                    _ => return Err(None),
                }
            }
        }
        _ => return Err(None),
    };
    let booking_time = Local::now().naive_local();
    let booking_status = status_values::BOOKED.to_owned();
    let booking_remarks = String::new();
    let (booking_user_id, booking_location) = match session.user_role {
        UserRole::Admin => (session.id, "Admin Office".to_owned()),
        UserRole::Client => (session.id, "Client Office".to_owned()),
        _ => unreachable!(),
    };

    let res = validate!(form, NewShipment, FormFields,
        {
            client_id: shipment_client_id,
            date_time: booking_time,
            invoice_id: None
        },
        (sender_name, |x: &str|
                if x.is_empty() {
                    Err("Sender name cannot be empty")
                } else {Ok(x.to_owned())}
        ),
        (sender_phone, |x: &str|
                if x.is_empty() {
                    Err("Sender phone cannot be empty")
            } else {Ok(x.to_owned())}
        ),
        (sender_addr, |x: &str|
                if x.is_empty() {
                    Err("Sender address cannot be empty")
            } else {Ok(x.to_owned())}
        ),
        (receiver_name, |x: &str|
                if x.is_empty() {
                    Err("Receiver name cannot be empty")
                } else {Ok(x.to_owned())}
        ),
        (receiver_phone, |x: &str|
                if x.is_empty() {
                    Err("Receiver phone cannot be empty")
            } else {Ok(x.to_owned())}
        ),
        (receiver_addr, |x: &str|
                if x.is_empty() {
                    Err("Receiver address cannot be empty")
            } else {Ok(x.to_owned())}
        ),
        (receiver_city, |x: &str|
                if x.is_empty() {
                    Err("City cannot be empty")
            } else {Ok(x.to_owned())}
        ),
        (receiver_area, |x: &str|
                if x.is_empty() {
                    Err("Area cannot be empty")
            } else {Ok(x.to_owned())}
        ),
        (delivery_charge, |x:&str|
            match x.parse::<f64>() {
                Err(_) => Err("Delivery charge must be a number"),
                Ok(x) => {
                    if x < 0.0 {
                        Err("Delivery charge must be greater than zero")
                    } else {
                        delivery_charge = Some(x);
                        Ok(x)
                    }
                }
            }
        ),
        (return_charge, |x:&str|
            match x.parse::<f64>() {
                Err(_) => Err("Return charge must be a number"),
                Ok(x) => {
                    if x < 0.0 {
                        Err("Return charge must be greater than zero")
                    } else {
                        return_charge = Some(x);
                        Ok(x)
                    }
                }
            }
        ),
        (material_cost, |x:&str|
            match x.parse::<f64>() {
                Err(_) => Err("Material cost must be a number"),
                Ok(x) => {
                    if x < 0.0 {
                        Err("Material cost must be greater than zero")
                    } else {
                        material_cost = Some(x);
                        Ok(x)
                    }
                }
            }
        ),
        (weight, |x:&str|
            match x.parse::<f64>() {
                Err(_) => Err("Weight must be a number"),
                Ok(x) => {
                    if x < 0.0 {
                        Err("Weight must be greater than zero")
                    } else {
                        weight = Some(x);
                        Ok(x)
                    }
                }
            }
        ),
        (pieces, |x:&str|
            match x.parse::<i32>() {
                Err(_) => Err("Pieces must be a number"),
                Ok(x) => {
                    if x < 0 {
                        Err("Pieces must be greater than zero")
                    } else {
                        pieces = Some(x);
                        Ok(x)
                    }
                }
            }
        ),
        (remarks, |x: &str| Ok(x.to_owned())),
        (spl_instrs, |x: &str| Ok(x.to_owned())),
        (your_ref, |x: &str| Ok(x.to_owned())),
        (your_company, |x: &str| Ok(x.to_owned()))
    );

    match res {
        Ok(new_shipment) => {
            let id = new_shipment.save(&db);
            let status = NewShipmentStatus {
                shipment_id: id,
                date_time: booking_time,
                location: booking_location,
                status: booking_status,
                remarks: booking_remarks,
                user_id: booking_user_id,
            };
            status.save(&db);
            Ok(Redirect::to(format!("/shipments/{}", id)))
        }
        Err(errors) => {
            let page = Page { form, errors };
            Err(Some(AppPage::with_template("shipments/book", page)))
        }
    }
}

fn search_sender(data: Form<FormFields>, db: Db) -> AppPage<Page> {
    let mut form = data.0;
    let mut errors = FormFields::default();

    form.client_id = String::new();
    form.sender_name = String::new();
    form.sender_phone = String::new();
    form.sender_addr = String::new();
    form.delivery_charge = String::new();
    form.return_charge = String::new();

    let mut done = false;

    let FormFields {
        search_sender_awb,
        search_sender_client_username,
        search_sender_phone,
        ..
    } = form;
    form.search_sender_awb = String::new();
    form.search_sender_client_username = String::new();
    form.search_sender_phone = String::new();

    if !done && !search_sender_awb.is_empty() {
        done = true;
        form.search_sender_awb = search_sender_awb.clone();
        // search awb
        if let Ok(awb) = search_sender_awb.parse() {
            let shipment = Shipment::by_id(awb, &db);
            if let Some(shipment) = shipment {
                let user = User::by_id(shipment.client_id, &db).unwrap();
                form.client_id = shipment.client_id.to_string();
                form.client_username = user.name;
                form.sender_name = shipment.sender_name;
                form.sender_phone = shipment.sender_phone;
                form.sender_addr = shipment.sender_addr;
                form.delivery_charge = shipment.delivery_charge.to_string();
                form.return_charge = shipment.return_charge.to_string();
                errors.search_sender_awb = "AWB found and details copied below".to_owned();
            } else {
                errors.search_sender_awb = "AWB not found".to_owned();
            }
        } else {
            errors.search_sender_awb = "Invalid AWB".to_owned();
        }
    }

    if !done && !search_sender_client_username.is_empty() {
        done = true;
        form.search_sender_client_username = search_sender_client_username.clone();
        // search by username
        let user = User::by_username(&search_sender_client_username, &db);
        if let Some(user) = user {
            form.client_id = user.id.to_string();
            form.client_username = user.name.clone();
            form.sender_name = user.name;
            form.sender_phone = user.phone;
            form.sender_addr = user.address;
            let client_info = ClientInfo::by_id(user.id, &db);
            if let Some(client_info) = client_info {
                form.delivery_charge = client_info.delivery_charge.to_string();
                form.return_charge = client_info.return_charge.to_string();
            }

            errors.search_sender_client_username =
                "Client found and details copied below".to_owned();
        } else {
            errors.search_sender_client_username = "Client not found".to_owned();
        }
    }

    if !done && !search_sender_phone.is_empty() {
        done = true;
        form.search_sender_phone = search_sender_phone.clone();
        let shipment = Shipment::by_sender_phone_last(&search_sender_phone, &db);
        if let Some(shipment) = shipment {
            let user = User::by_id(shipment.client_id, &db).unwrap();
            form.client_id = shipment.client_id.to_string();
            form.client_username = user.name;
            form.sender_name = shipment.sender_name;
            form.sender_phone = shipment.sender_phone;
            form.sender_addr = shipment.sender_addr;
            form.delivery_charge = shipment.delivery_charge.to_string();
            form.return_charge = shipment.return_charge.to_string();
            errors.search_sender_phone = "Sender details found and copied below".to_owned();
        } else {
            errors.search_sender_awb = "Sender not found".to_owned();
        }
    }

    drop(done);

    let page = Page { form, errors };
    AppPage::with_template("shipments/book", page)
}

fn clear_sender(data: Form<FormFields>, _db: Db) -> AppPage<Page> {
    let mut form = data.0;
    let errors = FormFields::default();
    form.client_id = "".to_owned();
    form.client_username = "".to_owned();
    let page = Page { form, errors };
    AppPage::with_template("shipments/book", page)
}

fn search_receiver(data: Form<FormFields>, db: Db) -> AppPage<Page> {
    let mut form = data.0;
    let mut errors = FormFields::default();

    let FormFields {
        search_receiver_awb,
        search_receiver_phone,
        ..
    } = form;
    form.search_receiver_awb = String::new();
    form.search_receiver_phone = String::new();

    let mut done = false;

    if !done && !search_receiver_awb.is_empty() {
        done = true;
        form.search_receiver_awb = search_receiver_awb.clone();
        // search awb
        if let Ok(awb) = search_receiver_awb.parse() {
            let shipment = Shipment::by_id(awb, &db);
            if let Some(shipment) = shipment {
                form.receiver_name = shipment.receiver_name;
                form.receiver_phone = shipment.receiver_phone;
                form.receiver_addr = shipment.receiver_addr;
                errors.search_receiver_awb = "AWB found and details copied below".to_owned();
            } else {
                errors.search_receiver_awb = "AWB not found".to_owned();
            }
        } else {
            errors.search_receiver_awb = "Invalid AWB".to_owned();
        }
    }

    if !done && !search_receiver_phone.is_empty() {
        done = true;
        form.search_receiver_awb = search_receiver_phone.clone();
        let shipment = Shipment::by_receiver_phone_last(&search_receiver_phone, &db);
        if let Some(shipment) = shipment {
            form.receiver_name = shipment.receiver_name;
            form.receiver_phone = shipment.receiver_phone;
            form.receiver_addr = shipment.receiver_addr;
            form.delivery_charge = shipment.delivery_charge.to_string();
            form.return_charge = shipment.return_charge.to_string();
            errors.search_receiver_phone = "Receiver details found and copied below".to_owned();
        } else {
            errors.search_receiver_awb = "Receiver not found".to_owned();
        }
    }

    drop(done);

    let page = Page { form, errors };
    AppPage::with_template("shipments/book", page)
}
