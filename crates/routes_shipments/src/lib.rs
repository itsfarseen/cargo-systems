#![feature(decl_macro)]

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate serde_derive;

use rocket::Route;

mod book;
mod bulk_upload;
mod scan;
mod track;

pub fn routes() -> Vec<Route> {
    let mut routes = Vec::new();
    routes.append(&mut scan::routes());
    routes.append(&mut track::routes());
    routes.append(&mut bulk_upload::routes());
    routes.append(&mut book::routes());
    routes
}
