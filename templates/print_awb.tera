{% extends "base" %}
<!-- vim:ft=html.jinja2: 
!-->

<!-- -->
{% block title %}Print AWB{% endblock title %}

<!-- -->
{% block body %}

<body class="flex flex-col items-center print:p-0">
  <script src="/static/qrcode.js"></script>
  <style>
    :root {
      --margin: 1cm;
      --width: 21cm;
      --height: 29.7cm;
      --gap: 2cm;
    }

    .name,
    .address {
      font-family: MarkaziText;
      line-height: 1.1;
      font-size: 1.2rem;
    }

    @media screen {
      .awb-group {
        max-width: calc(var(--width) - 2 * var(--margin));
        max-height: calc(var(--height) - 2 * var(--margin) - var(--gap))
      }
    }

    @page {
      /* Will be used by print window to set default margins. */
      margin: 1cm;
    }

    .awb-group {
      min-width: calc(var(--width) - 2 * var(--margin));
      min-height: calc((var(--height) - 2 * var(--margin) - var(--gap)));
      /* max-width: calc(var(--width) - 2 * var(--margin)); */
      /* max-height: calc((var(--height) - 2 * var(--margin) - var(--gap)) / 2); */
      page-break-after: always;
    }
  </style>
  <div class="print:hidden flex justify-center fixed">
    <button class="bg-red-600 py-2 px-4 text-white hover:bg-red-500 active:bg-red-600"
      onclick="window.print()">Print</button>
  </div>
  <div class="print:hidden mb-3 flex">
    <!-- to give space for print button initially -->
  </div>
  {% set label_class = "text-xs text-gray-700" %}
  <!-- -->
  {% for shipment in shipments %}
  <!-- -->
  <div class="flex flex-col gap-5 awb-group p-10">
    {% for i in [1,2] %}
    <div class="awb flex-1 border-gray-500 border flex flex-col divide-y" style="max-height: calc(50% - 5em);">

      <!-- Header - Logo and QRCode -->
      <div class="flex items-start p-3">
        <div class="flex flex-1 gap-5">
          <img class="object-contain w-48 h-20" src="/static/logo.jpeg" />
          <div class="flex-1 self-center text-xs">
              {% set c = config() %}
              {% for line in c.company_address %}
                <p>{{line}}</p>
              {% endfor %}
          </div>
        </div>
        <div class="flex flex-col gap-2">
          <div id="qrCode{{shipment.id}}_{{i}}" class="border-red-500"></div>
          <div class="flex flex-col items-center leading-none">
            <div class="text-sm font-bold">AWB {{shipment.id}}</div>
          </div>
        </div>
      </div>

      <!-- Body -->
      <div class="flex-1 flex divide-x">
        <!-- Left Column -->
        <div class="flex-1 p-3 flex flex-col gap-2">
          <div class="flex gap-5">
            <div class="flex flex-col">
              <div class="{{label_class}}">Booking Date </div>
              <div>{{shipment.date_time | date(format="%d-%m-%Y")}}</div>
            </div>
            <div class="flex flex-col">
              <div class="{{label_class}}">Sender's Ref</div>
              <div>{{shipment.your_ref}}</div>
            </div>
          </div>
          <div class="flex-1 flex flex-col gap-3">
            <div class="flex-1 flex flex-col">
              <div class="{{label_class}}">Sender</div>
              <div>{{shipment.sender_name}}</div>
              <div>{{shipment.sender_phone}} </div>
              <div>{{shipment.sender_addr}}</div>
            </div>
            <div class="flex flex-col">
              <div class="{{label_class}}">Special Instructions </div>
              <div>{{shipment.spl_instrs}}</div>
            </div>
          </div>
        </div>

        <!-- Mid Column -->
        <div class="flex-1 p-3 flex flex-col gap-2">
          <div class="">
            <div class="{{label_class}}">Receiver's Name</div>
            <div class="name">{{shipment.receiver_name}} </div>
          </div>
          <div class="flex-1 flex flex-col">
            <div class="{{label_class}}">Address</div>
            <div class="address">{{shipment.receiver_addr | replace(from=",", to="<br>")}}</div>
          </div>
        </div>

        <!-- Right Column -->
        <div class="flex-1 p-3 flex flex-col gap-2">
          <div class="flex-1 flex flex-col gap-2">
            <div>
              <div class="{{label_class}}">Receiver's Phone</div>
              <div>{{shipment.receiver_phone}} </div>
            </div>
            <div>
              <div class="{{label_class}}">Area</div>
              <div class=>{{shipment.receiver_area}}</div>
            </div>
            <div>
              <div class="{{label_class}}">City</div>
              <div class=>{{shipment.receiver_city}}</div>
            </div>
          </div>
          <div class="flex gap-3 text-sm">
            <div class="flex flex-col">
              <div class="{{label_class}}">Material Cost </div>
              <div>{{shipment.material_cost}} AED</div>
            </div>
            <div class="flex flex-col">
              <div class="{{label_class}}">Weight </div>
              <div>{{shipment.weight}} Kg</div>
            </div>
            <div class="flex flex-col">
              <div class="{{label_class}}">Pieces </div>
              <div>{{shipment.pieces}} Pcs</div>
            </div>
          </div>
        </div>
      </div>

      <!-- Footer -->
      <div class="flex flex-0 border-t border-1 divide-x">
        <div class="flex flex-1 items-end p-3 pt-10">
          <div class="{{label_class}} flex-1">Delivery by</div>
          <div class="{{label_class}}">Date &amp; Time</div>
        </div>
        <div class="flex flex-1 items-end p-3 pt-10">
          <div class="{{label_class}} flex-1">Collected by</div>
          <div class="{{label_class}}">Date &amp; Time</div>
        </div>
      </div>

    </div>
    {% endfor %}
  </div>
  <!-- -->
  <script>
    var typeNumber = 0;
    var errorCorrectionLevel = 'H';
    var qr = qrcode(typeNumber, errorCorrectionLevel);
    qr.addData('{{shipment.id}}');
    qr.make();
    console.log('{{loop.index}}');
    document.getElementById('qrCode{{shipment.id}}_1').innerHTML = qr.createImgTag(
      3, 0);
    document.getElementById('qrCode{{shipment.id}}_2').innerHTML = qr.createImgTag(
      3, 0);
  </script>
  {% endfor %}
</body>
{% endblock body %}
