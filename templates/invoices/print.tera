{% extends "base" %}
<!-- vim:ft=html.jinja2: 
!-->

<!-- -->
{% block title %}Invoice {{invoice.id}} - {{client_name}}{% endblock title %}

<!-- -->
{% block body %}

<body class="print:p-0">
  <style>
    :root {
      --margin: 1cm;
      --width: 21cm;
      --height: 29.7cm;
      --gap: 2cm;
      font-size: 0.8rem;
    }

    td {
      overflow: hidden;
      padding: 0.3em 0.2em;
      vertical-align: top;
    }

    th:first-child,
    td:first-child {
      padding-left: 0.3em;
    }

    th:last-child,
    td:last-child {
      padding-right: 0.3em;
    }

    thead td {
      padding: 0 !important;
    }

    th {
      white-space: nowrap;
      text-align: left;
      padding: 0.3em 0.2em;
    }

    .name,
    .address {
      font-family: MarkaziText;
      line-height: 1.1;
      font-size: 1.2rem;
    }

    @media screen {
      .container {
        max-width: calc(var(--width) - 2 * var(--margin));
      }
    }

    @page {
      /* Will be used by print window to set default margins. */
      margin: 1cm;
    }

    .container {
      min-width: calc(var(--width) - 2 * var(--margin));
      /* max-width: calc(var(--width) - 2 * var(--margin)); */
      /* max-height: calc((var(--height) - 2 * var(--margin) - var(--gap)) / 2); */
      margin: auto;
    }
  </style>
  <div class="print:hidden w-full flex justify-center fixed">
    <button class="bg-red-600 py-2 px-4 text-white hover:bg-red-500 active:bg-red-600"
      onclick="window.print()">Print</button>
  </div>
  <div class="print:hidden h-16 flex">
    <!-- to give space for print button initially -->
  </div>
  <!-- -->
  <div class="container flex-1 border-gray-400 border-l border-r">
    <div>
            <!-- Header - Logo and Client Details -->
            <div class="flex items-start p-3 border-b border-t">
              <img class="object-contain w-48 h-20" src="/static/logo.jpeg" />
              <div class="flex-1 flex flex-col self-center items-center">
                <div class="font-bold text-2xl leading">Invoice #{{invoice.id}}</div>
                <div class="text-sm">{{client_name}} - {{invoice.date_time | date(format="%d/%m/%y")}}</div>
              </div>
              <div class="self-center flex flex-col items-end text-xs">
                {% set c = config() %}
                {% for line in c.company_address %}
                  <p>{{line}}</p>
                {% endfor %}
              </div>
            </div>

            <!-- Header - Logo and QRCode -->
      <div class="flex flex-col space-y-4 border-b border-grey-100 p-4">
        <div>
          <div class="label">Total Invoice</div>
          <div class="text-2xl font-semibold">{{ invoice.invoice_amt | round(precision=2) }} AED</div>
        </div>
        <div class="flex sm:flex-col sm:space-y-4">
          <div class="flex-1 flex flex-col space-y-2 sm:flex-row sm:space-x-2 sm:space-y-0">
            <div class="flex-1">
              <div class="label">Deliveries</div>
              <div>{{ invoice.num_deliveries }}</div>
            </div>
            <div class="flex-1">
              <div class="label">Sum Delivery Charge</div>
              <div> {{ invoice.vat_incl_sum_delivery_charge | round(precision=2) }} AED</div>
              <div class="text-sm">{{ invoice.sum_delivery_charge | round(precision=2) }} AED + {{ invoice.vat_pct | round(precision=2) }}% VAT</div>
            </div>
          </div>
          <div class="flex flex-col flex-1 space-y-2 sm:flex-row sm:space-x-2 sm:space-y-0">
            <div class="flex-1">
              <div class="label">Returns</div>
              <div> {{ invoice.num_returns }}</div>
            </div>
            <div class="flex-1">
              <div class="label">Sum Return Charge</div>
              <div>{{ invoice.vat_incl_sum_return_charge | round(precision=2) }} AED </div>
              <div class="text-sm">{{ invoice.sum_return_charge | round(precision=2) }} AED + {{ invoice.vat_pct | round(precision=2) }}% VAT </div>
            </div>
          </div>
        </div>
        <div class="flex">
          <div class="flex-1">
            <div class="label">Other Shipments</div>
            <div>{{ invoice.num_in_transit }}</div>
          </div>
          <div class="flex-1">
            <div class="label">Material Costs
            </div>
            <div>{{ invoice.sum_material_cost | round(precision=2) }} AED
            </div>
          </div>
        </div>
        <div class="flex flex-1">
          <div class="flex flex-1">
            <div class="flex-1">
              <div class="label">Total Shipments</div>
              <div>{{ invoice.num_shipments }}
              </div>
            </div>
          </div> 
          <div class="flex-1">
          </div>
        </div>
        <div class="flex">
        </div>
      </div>
    </div>
    <table class="w-full">
      <thead>
        <tr class="text-sm border-t">
          <th>AWB#</th>
          <th>Booked On</th>
          <th>Sender Ref</th>
          <th>Receiver</th>
          <th>Phone</th>
          <th>Status</th>
          <th>Updated On</th>
          <th>M.Cost</th>
        </tr>
      </thead>
      <tbody class="text-sm">
        {% for item in shipments %}
        <!-- -->
        {% set shipment = item[0] %}
        <!-- -->
        {% set status = item[1] %}
        <!-- -->
        <tr class="">
          <td>{{shipment.id}}</td>
          <td>{{shipment.date_time | date(format="%d/%m/%y")}}</td>
          <td>{{shipment.your_ref}}</td>
          <td class="whitespace-wrap">{{shipment.receiver_name}}</td>
          <td>{{shipment.receiver_phone}}</td>
          {% if status %}
          <td>{{status.status}}</td>
          <td>{{status.date_time | date(format="%d/%m/%y")}}</td>
          {% else %}
          <td>N/A</td>
          <td>N/A</td>
          {% endif %}
          <td class="whitespace-no-wrap">{{shipment.material_cost}} AED</td>
        </tr>

        {% endfor %}
      </tbody>
      <tfoot class="border-b mt-4">
        <td colspan="9"></td>
      </tfoot>
    </table>
  </div>


</body>
{% endblock body %}
