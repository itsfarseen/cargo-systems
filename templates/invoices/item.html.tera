{% extends "app_page_base" %}
<!-- -->
{% import "macros/shipments_list" as shipments_list %}
<!-- -->
{% block title %} Invoice #{{page.invoice.id}} {% endblock title %}
<!-- -->
<!-- -->
{% block content %}
<script>
  function toggleDelete() {
    let deleteForm = document.getElementById("deleteForm");
    deleteForm.classList.toggle("hidden");
  }
</script>

<div class="sheet">

  <div class="flex">
    <div class="flex-1">
      <h1 class="card-title">{{ page.client_name }}</h1>
      <h2 class="card-subtitle">Invoice</h2>
    </div>
    <div class="flex items-center gap-4">
      <button class="btn" onclick="toggleDelete()">Delete</button>
      <a href="/invoices/{{page.invoice.id}}?print" class="btn">Print</a>
    </div>
  </div>

  <form id="deleteForm" 
        class="hidden -- card-section sm:flex-row -- items-baseline flex-wrap -- bg-red-200" 
        method="POST" 
        action="/invoices/{{page.invoice.id}}?delete">
    <div class="flex-1 flex flex-wrap">
      <p>Are you sure to delete this invoice?&nbsp;</p>
      <p>This action is <u>cannot be undone.</u></p>
    </div>
    <div class="flex gap-2">
      <button class="btn-danger">Confirm Delete</button>
      <button class="btn" type="button" onclick="toggleDelete()">Cancel</button>
    </div>
  </form>

  <!-- Invoice Details -->
  <div class="card-section">

    <div>
      <div class="label">Invoice</div>
      <div class="text-2xl font-semibold">{{ page.invoice.invoice_amt | round(precision=2) }} AED</div>
    </div>

    <!-- Delivery, returns -->
    <div class="flex gap-2">
      <div class="flex flex-col gap-2 flex-1">
        <div class="flex-1">
          <div class="label">Deliveries</div>
          <div>{{ page.invoice.num_deliveries }}</div>
        </div>
        <div class="flex-1">
          <div class="label">Sum Delivery Charge</div>
          <div> {{ page.invoice.vat_incl_sum_delivery_charge | round(precision=2) }} AED</div>
          <div class="text-sm">{{ page.invoice.sum_delivery_charge | round(precision=2) }} AED + {{ page.invoice.vat_pct | round(precision=2) }}% VAT</div>
        </div>
      </div>

      <div class="flex flex-col gap-2 flex-1">
        <div class="flex-1">
          <div class="label">Returns</div>
          <div> {{ page.invoice.num_returns }}</div>
        </div>
        <div class="flex-1">
          <div class="label">Sum Return Charge</div>
          <div>{{ page.invoice.vat_incl_sum_return_charge | round(precision=2) }} AED </div>
          <div class="text-sm">{{ page.invoice.sum_return_charge | round(precision=2) }} AED + {{ page.invoice.vat_pct | round(precision=2) }}% VAT </div>
        </div>
      </div>
    </div>

    <!-- Others -->
    <div class="flex">
      <div class="flex-1">
        <div class="label">Other Shipments</div>
        <div>{{ page.invoice.num_in_transit }}</div>
      </div>
      <div class="flex-1">
        <div class="label">Total Shipments</div>
        <div>{{ page.invoice.num_shipments }}
        </div>
      </div>
    </div>

    <!-- Costs -->
    <div class="flex">
      <div class="flex-1">
        <div class="label">Material Costs</div>
        <div>{{ page.invoice.sum_material_cost | round(precision=2) }} AED</div>
      </div>
      <div class="flex-1">
        <div class="label">Net Payable</div>
        <div>{{ page.invoice.net_payable | round(precision=2) }} AED</div>
      </div>
    </div>

  </div>

  <div class="card-section p-0">
    <div class="px-4">
      <h1 class="card-title">Shipments in this Invoice</h1>
    </div>
    {{ shipments_list::shipments_list(shipments=page.shipments) }}
  </div>

</div>
{% endblock content %}
