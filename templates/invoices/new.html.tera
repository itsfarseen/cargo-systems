{% extends "app_page_base" %}
<!-- -->
{% import "macros/shipments_list" as shipments_list %}
<!-- -->
{% block title %} New Invoice {% endblock title %}
<!-- -->
<!-- -->
{% block content %}
{% set this_page_url =  "/invoices/new?client=" ~ page.client_id 
%}
<div class="sheet gap-0">

  <div>
    <h1 class="card-title">{{ page.client_name }}</h1>
    <h2 class="card-subtitle">New Invoice</h2>
  </div>

  <form class="card-section md:flex-row" method="GET">
    <input name="client" type="hidden" value="{{ page.client_id }}" /> 
    <input type="hidden" name="action" value="search" />
    <div class="flex flex-col flex-1">
      <label class="label" for="from_date">From</label>
      <input class="input" id="from_date" name="from_date" type="date" value="{{ page.from_date }}"
      />
    </div>
    <div class="flex flex-col flex-1">
      <label class="label" for="to_date">To</label>
      <input class="input" id="to_date" name="to_date" type="date" value="{{ page.to_date }}" />
    </div>
    <div class="flex flex-col flex-1">
      <label class="label" for="filter">Filter</label>
      <select class="input" id="filter" name="filter" type="" value="{{ page.shipments_filter }}">
        {% for filter in page.shipments_filters %}
        <option value="{{filter}}"
          {% if filter == page.shipments_filter %}selected{% endif %}
        >{{filter}}</option>
        {% endfor %}
      </select>
    </div>
    <div class="flex items-end flex-1">
      <input class="flex-1 btn-primary" type="submit" value="Search" />
    </div>
  </form>

  <script>
    function update_tabs(elem) {                    
      let unselected_class=["hover:bg-green-100", "text-gray-700", "hover:border-green-400"];
      let selected_class=["text-green-700", "border-green-500"];
      let search_results_radio = document.getElementById("search_results_radio");
      let search_results_tab = document.querySelector("label[for=search_results_radio]");
      let search_results_content = document.getElementById("search_results_content");
      let invoice_radio = document.getElementById("invoice_radio");
      let invoice_tab = document.querySelector("label[for=invoice_radio]");
      let invoice_content = document.getElementById("invoice_content");

      let none_checked = true;
      if(search_results_radio.checked) {
        search_results_content.classList.remove("hidden");
        search_results_tab.classList.remove(...unselected_class);
        search_results_tab.classList.add(...selected_class);
        none_checked = false;
      } else {
        search_results_content.classList.add("hidden");
        search_results_tab.classList.remove(...selected_class);
        search_results_tab.classList.add(...unselected_class);
      }
      if(invoice_radio.checked) {
        invoice_content.classList.remove("hidden");
        invoice_tab.classList.remove(...unselected_class);
        invoice_tab.classList.add(...selected_class);
        none_checked = false;
      } else {
        invoice_content.classList.add("hidden");
        invoice_tab.classList.remove(...selected_class);
        invoice_tab.classList.add(...unselected_class);
      }

      if(none_checked) {
        search_results_radio.checked = true;
        update_tabs(elem);
      }
    }

    function select_all(formelem) {
      let search_results_content = document.getElementById(formelem);
      let form = search_results_content;
      for(element of form.elements) {
        if(element.type==="checkbox") {
          element.checked=true;
        }
      }
    }

    function select_none(formelem) {
      let search_results_content = document.getElementById(formelem);
      let form = search_results_content;
      for(element of form.elements) {
        if(element.type==="checkbox") {
          element.checked=false;
        }
      }
    }

    function onload() {

      let search_results_radio = document.getElementById("search_results_radio");
      let invoice_radio = document.getElementById("invoice_radio");

      let url = new URL(document.location.toString());
      if(url.searchParams.get("action")=="search") {
        search_results_radio.checked = true;
        invoice_radio.checked = false;
      } else {
        search_results_radio.checked = false;
        invoice_radio.checked = true;
      }

      update_tabs()

      let addBtn = document.querySelector("#addBtn")
      url.searchParams.set("action", "add");
      addBtn.formAction = url.toString();

      let removeBtn = document.querySelector("#removeBtn")
      url.searchParams.set("action", "remove");
      removeBtn.formAction = url.toString();
    }

    document.body.onload = onload;
  </script>

  <div class="flex flex-col -mx-4">
    <div class="flex items-stretch">
      {% set tab_class = 
        "flex-1 flex 
          justify-center items-stretch 
          font-bold
          border-b-2 py-4" %}
      <input 
        class="hidden" 
        type="radio" 
        name="tab_selector" 
        id="search_results_radio" 
        onchange="update_tabs(this)"
      />
      <label for="search_results_radio" class="{{tab_class}}">Search Results ({{page.search_results | length}})</label>
      <input 
        class="hidden" 
        type="radio" 
        name="tab_selector" 
        id="invoice_radio"
        onchange="update_tabs(this)"
      />
      <label for="invoice_radio" class="{{tab_class}}">Selected for Invoice ({{page.staged_shipments | length}})</label>
    </div>
    <form id="search_results_content" class="flex flex-col">
      <div class="flex p-2 space-x-2">
        <div class="flex-1 space-x-2">
          <button class="btn" type="button" onclick="select_all('search_results_content')">Select All</button>
          <button class="btn" type="button" onclick="select_none('search_results_content')">Select None</button>
          <button id="addBtn"
                  class="btn" 
                  type="submit" 
                  {# formaction="{{this_page_url}}&action=add" #}
                  formmethod="POST">Add Selected</button>
        </div>
        <div class="">
        </div>
      </div>
      {{ shipments_list::shipments_list(shipments=page.search_results, selectable=true) }}
    </form>
    <form id="invoice_content" class="flex flex-col">
      <div class="flex p-2 space-x-2">
        <div class="flex-1 space-x-2">
          <button class="btn" type="button" onclick="select_all('invoice_content')">Select All</button>
          <button class="btn" type="button" onclick="select_none('invoice_content')">Select None</button>
          <button id="removeBtn"
                  class="btn" 
                  type="submit" 
                  formmethod="POST" 
                  {# formaction="{{this_page_url}}&action=remove" #}
                  >Remove Selected</button>
        </div>
        <div class="">
          <button class="btn-primary" 
                  type="submit" 
                  formmethod="POST" 
                  formaction="new?client={{page.client_id}}&action=generate">Generate Invoice</button>
        </div>
      </div>
      {{ shipments_list::shipments_list(shipments=page.staged_shipments, selectable=true) }}
    </form>
  </div>

</div>
{% endblock content %}
