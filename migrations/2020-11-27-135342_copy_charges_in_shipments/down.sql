-- This file should undo anything in `up.sql`
ALTER TABLE shipments
    DROP COLUMN delivery_charge,
    DROP COLUMN return_charge;