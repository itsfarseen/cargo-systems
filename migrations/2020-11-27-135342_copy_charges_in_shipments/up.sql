-- Your SQL goes here
ALTER TABLE shipments
    ADD COLUMN delivery_charge DOUBLE PRECISION,
    ADD COLUMN return_charge DOUBLE PRECISION;

UPDATE shipments 
    SET (delivery_charge, return_charge) = 
        (SELECT delivery_charge,return_charge FROM client_info where client_info.client_id = shipments.client_id) 
    WHERE delivery_charge IS NULL;

ALTER TABLE shipments
    ALTER COLUMN delivery_charge SET NOT NULL,
    ALTER COLUMN return_charge SET NOT NULL;