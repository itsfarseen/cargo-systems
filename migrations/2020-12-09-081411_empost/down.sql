-- This file should undo anything in `up.sql`
ALTER TABLE shipments
    DROP CONSTRAINT fk_shipments_empost;

ALTER TABLE shipments DROP COLUMN empost_id;

DROP TABLE emposts;