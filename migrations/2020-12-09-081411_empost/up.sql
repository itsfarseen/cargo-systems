-- Your SQL goes here
CREATE TABLE "emposts" (
    "id" SERIAL NOT NULL,
    "date_time" TIMESTAMP NOT NULL,
    "from_date" DATE NOT NULL,
    "to_date" DATE NOT NULL,
    "notes" TEXT NOT NULL,

    "vat_pct" DOUBLE PRECISION NOT NULL,
    "empost_pct" DOUBLE PRECISION NOT NULL,

    "empost_min_delivery_charge" DOUBLE PRECISION NOT NULL,
    "empost_max_weight" DOUBLE PRECISION NOT NULL,

    "num_shipments" INT NOT NULL,

    "above_weight_num_shipments" INT NOT NULL,
    "above_weight_sum_delivery_charge" DOUBLE PRECISION NOT NULL,
    "above_weight_empost_amt" DOUBLE PRECISION NOT NULL,
    "above_weight_empost_vat" DOUBLE PRECISION NOT NULL,

    "below_min_charge_num_shipments" INT NOT NULL,
    "below_min_charge_sum_delivery_charge" DOUBLE PRECISION NOT NULL,
    "below_min_charge_empost_amt" DOUBLE PRECISION NOT NULL,
    "below_min_charge_empost_vat" DOUBLE PRECISION NOT NULL,

    "other_num_shipments" INT NOT NULL,
    "other_sum_delivery_charge" DOUBLE PRECISION NOT NULL,
    "other_empost_amt" DOUBLE PRECISION NOT NULL,
    "other_empost_vat" DOUBLE PRECISION NOT NULL,


    "sum_delivery_charge" DOUBLE PRECISION NOT NULL,  
    "sum_empost_amt" DOUBLE PRECISION NOT NULL,  
    "sum_empost_vat" DOUBLE PRECISION NOT NULL,  

    PRIMARY KEY ("id")
);

ALTER TABLE shipments ADD COLUMN "empost_id" INT;

ALTER TABLE shipments
    ADD CONSTRAINT fk_shipments_empost
    FOREIGN KEY(empost_id) REFERENCES emposts(id)
    ON UPDATE CASCADE
    ON DELETE SET NULL;