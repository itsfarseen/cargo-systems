-- Your SQL goes here
ALTER TABLE users
ADD COLUMN address TEXT;

ALTER TABLE users
ADD COLUMN phone TEXT;

UPDATE users
SET address='', phone='';

ALTER TABLE users
ALTER COLUMN address SET NOT NULL;

ALTER TABLE users
ALTER COLUMN phone SET NOT NULL;

ALTER TABLE client_info
DROP COLUMN address;
