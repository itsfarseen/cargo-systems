-- This file should undo anything in `up.sql`
ALTER TABLE users
DROP COLUMN address;

ALTER TABLE users
DROP COLUMN phone;

ALTER TABLE client_info
ADD COLUMN address TEXT;

UPDATE client_info
SET address='';

ALTER TABLE client_info
ALTER COLUMN address SET NOT NULL;
