-- Your SQL goes here
ALTER TABLE shipment_status
    ADD CONSTRAINT fk_shipment_status_shipment
        FOREIGN KEY(shipment_id) REFERENCES shipments(id)
        ON UPDATE CASCADE
        ON DELETE CASCADE;

