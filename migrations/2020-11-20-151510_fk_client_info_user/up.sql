-- Your SQL goes here
ALTER TABLE client_info 
    ADD CONSTRAINT fk_client_info_user 
        FOREIGN KEY(client_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE CASCADE;

