-- This file should undo anything in `up.sql`
ALTER TABLE shipments RENAME TO consignments;
ALTER TABLE shipment_status RENAME TO consignment_status;

ALTER TABLE consignments RENAME COLUMN receiver_name TO consignee_name;
ALTER TABLE consignments RENAME COLUMN receiver_phone TO consignee_phone;
ALTER TABLE consignments RENAME COLUMN receiver_addr TO consignee_addr;
ALTER TABLE consignments RENAME COLUMN receiver_city TO consignee_city;
ALTER TABLE consignments RENAME COLUMN receiver_area TO consignee_area;

ALTER TABLE consignment_status RENAME COLUMN shipment_id TO consignment_id;
