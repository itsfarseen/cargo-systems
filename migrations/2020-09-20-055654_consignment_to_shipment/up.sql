-- Your SQL goes here
ALTER TABLE consignments RENAME COLUMN consignee_name TO receiver_name;
ALTER TABLE consignments RENAME COLUMN consignee_phone TO receiver_phone;
ALTER TABLE consignments RENAME COLUMN consignee_addr TO receiver_addr;
ALTER TABLE consignments RENAME COLUMN consignee_city TO receiver_city;
ALTER TABLE consignments RENAME COLUMN consignee_area TO receiver_area;

ALTER TABLE consignment_status RENAME COLUMN consignment_id TO shipment_id;

ALTER TABLE consignments RENAME TO shipments;
ALTER TABLE consignment_status RENAME TO shipment_status;
