-- Your SQL goes here
CREATE TABLE "clients" (
	"id" SERIAL NOT NULL,
	"name" VARCHAR(255) NOT NULL,
	"username" VARCHAR(255) NOT NULL,
	"hashword" VARCHAR(255) NOT NULL,
	"delivery_charge" INT NOT NULL,
	"return_charge" INT NOT NULL,
	UNIQUE ("username"),
	PRIMARY KEY ("id")
);
