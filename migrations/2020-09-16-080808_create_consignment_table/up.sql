-- Your SQL goes here
CREATE TABLE "consignments" (
	"id" SERIAL NOT NULL,
	"client_id" INT NOT NULL,
	"date_time" TIMESTAMP NOT NULL,
	"your_ref" VARCHAR(255) NOT NULL,
	"consignee_name" VARCHAR(255) NOT NULL,
	"consignee_phone" VARCHAR(255) NOT NULL,
	"consignee_addr" VARCHAR(255) NOT NULL,
	"consignee_city" VARCHAR(255) NOT NULL,
	"consignee_area" VARCHAR(255) NOT NULL,
	"weight" DOUBLE PRECISION NOT NULL,
	"pieces" INT NOT NULL,
	"material_cost" DOUBLE PRECISION NOT NULL,
	"spl_instrs" VARCHAR(255) NOT NULL,
	"remarks" VARCHAR(255) NOT NULL,
	"your_company" VARCHAR(255) NOT NULL,
	PRIMARY KEY ("id")
);
