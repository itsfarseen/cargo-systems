-- Your SQL goes here
ALTER TABLE shipments
    ADD COLUMN sender_name TEXT,
    ADD COLUMN sender_addr TEXT,
    ADD COLUMN sender_phone TEXT;

UPDATE shipments SET (sender_name, sender_addr, sender_phone) = (SELECT name,address,phone FROM users where users.id = shipments.client_id) WHERE sender_name IS NULL;

ALTER TABLE shipments
    ALTER COLUMN sender_name SET NOT NULL,
    ALTER COLUMN sender_addr SET NOT NULL,
    ALTER COLUMN sender_phone SET NOT NULL;