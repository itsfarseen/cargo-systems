-- This file should undo anything in `up.sql`
ALTER TABLE shipments
    DROP COLUMN sender_name,
    DROP COLUMN sender_addr,
    DROP COLUMN sender_phone;