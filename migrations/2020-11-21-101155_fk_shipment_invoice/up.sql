-- Your SQL goes here
ALTER TABLE shipments
    ADD CONSTRAINT fk_shipments_invoice
    FOREIGN KEY(invoice_id) REFERENCES invoices(id)
    ON UPDATE CASCADE
    ON DELETE SET NULL;
