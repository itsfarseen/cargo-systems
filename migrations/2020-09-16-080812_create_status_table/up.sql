-- Your SQL goes here
CREATE TABLE "consignment_status" (
	"id" SERIAL NOT NULL,
	"consignment_id" INT NOT NULL,
	"date_time" TIMESTAMP NOT NULL,
	"location" VARCHAR(255) NOT NULL,
	"status" VARCHAR(255) NOT NULL,
	"remarks" VARCHAR(255) NOT NULL,
	PRIMARY KEY ("id")
);
