-- This file should undo anything in `up.sql`
ALTER SEQUENCE shipment_status_id_seq RENAME TO consignment_status_id_seq;
ALTER SEQUENCE shipments_id_seq RENAME TO consignments_id_seq;
