-- Your SQL goes here
ALTER TABLE shipments 
    ADD CONSTRAINT fk_shipments_user 
        FOREIGN KEY(client_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE CASCADE;
