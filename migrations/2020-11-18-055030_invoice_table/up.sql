-- Your SQL goes here
CREATE TABLE "invoices" (
    "id" SERIAL NOT NULL,
    "client_id" INT NOT NULL,
    "date_time" TIMESTAMP NOT NULL,
    "notes" TEXT NOT NULL,

    "sum_material_cost" DOUBLE PRECISION NOT NULL,
    "vat_pct" DOUBLE PRECISION NOT NULL,

    "num_deliveries" INT NOT NULL,
    "delivery_charge" DOUBLE PRECISION NOT NULL,
    "delivery_charge_vat_incl" DOUBLE PRECISION NOT NULL,
    "sum_delivery_charge" DOUBLE PRECISION NOT NULL,

    "num_returns" INT NOT NULL,
    "return_charge" DOUBLE PRECISION NOT NULL,
    "return_charge_vat_incl" DOUBLE PRECISION NOT NULL,
    "sum_return_charge" DOUBLE PRECISION NOT NULL,

    "num_in_transit" INT NOT NULL,

    "num_shipments" INT NOT NULL,

    "invoice_amt" DOUBLE PRECISION NOT NULL,  
    "net_payable" DOUBLE PRECISION NOT NULL,  

    PRIMARY KEY ("id")
);

ALTER TABLE shipments ADD COLUMN "invoice_id" INT;
