-- This file should undo anything in `up.sql`
ALTER TABLE client_info
    ALTER COLUMN delivery_charge TYPE INT,
    ALTER COLUMN return_charge TYPE INT;