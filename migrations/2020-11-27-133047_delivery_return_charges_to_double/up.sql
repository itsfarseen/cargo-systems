-- Your SQL goes here
ALTER TABLE client_info
    ALTER COLUMN delivery_charge TYPE DOUBLE PRECISION,
    ALTER COLUMN return_charge TYPE DOUBLE PRECISION;