-- Your SQL goes here
CREATE TABLE "admins" (
	"id" SERIAL NOT NULL,
	"name" VARCHAR(255) NOT NULL,
	"username" VARCHAR(255) NOT NULL,
	"hashword" VARCHAR(255) NOT NULL,
	UNIQUE ("username"),
	PRIMARY KEY ("id")
);
