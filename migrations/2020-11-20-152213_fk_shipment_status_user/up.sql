-- Your SQL goes here
ALTER TABLE shipment_status
    ADD CONSTRAINT fk_shipment_status_user 
        FOREIGN KEY(user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE CASCADE;
