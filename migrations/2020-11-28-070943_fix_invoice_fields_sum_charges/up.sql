-- Your SQL goes here
ALTER TABLE invoices
    ADD COLUMN vat_incl_sum_delivery_charge DOUBLE PRECISION,
    ADD COLUMN vat_incl_sum_return_charge DOUBLE PRECISION;

UPDATE invoices SET 
    vat_incl_sum_delivery_charge = vat_pct * sum_delivery_charge,
    vat_incl_sum_return_charge = vat_pct * sum_return_charge
    WHERE vat_incl_sum_delivery_charge IS NULL;

ALTER TABLE invoices
    ALTER COLUMN vat_incl_sum_delivery_charge SET NOT NULL,
    ALTER COLUMN vat_incl_sum_return_charge SET NOT NULL;

ALTER TABLE invoices
    DROP COLUMN delivery_charge,
    DROP COLUMN return_charge,
    DROP COLUMN delivery_charge_vat_incl,
    DROP COLUMN return_charge_vat_incl;