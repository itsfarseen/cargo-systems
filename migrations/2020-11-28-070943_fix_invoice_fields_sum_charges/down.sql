-- This file should undo anything in `up.sql`
ALTER TABLE invoices
    ADD COLUMN delivery_charge DOUBLE PRECISION,
    ADD COLUMN return_charge DOUBLE PRECISION,
    ADD COLUMN delivery_charge_vat_incl DOUBLE PRECISION,
    ADD COLUMN return_charge_vat_incl DOUBLE PRECISION;


UPDATE invoices SET 
    delivery_charge = sum_delivery_charge / num_deliveries,
    return_charge = sum_return_charge / num_returns,
    delivery_charge_vat_incl = vat_incl_sum_delivery_charge / num_deliveries,
    return_charge_vat_incl = vat_incl_sum_return_charge / num_returns
    WHERE delivery_charge IS NULL;

ALTER TABLE invoices
    ALTER COLUMN delivery_charge SET NOT NULL,
    ALTER COLUMN return_charge SET NOT NULL,
    ALTER COLUMN delivery_charge_vat_incl SET NOT NULL,
    ALTER COLUMN return_charge_vat_incl SET NOT NULL;

ALTER TABLE invoices
    DROP COLUMN vat_incl_sum_delivery_charge,
    DROP COLUMN vat_incl_sum_return_charge;