-- Your SQL goes here

CREATE TABLE clients (
    id SERIAL PRIMARY KEY NOT NULL,
    name VARCHAR(255) NOT NULL,
    username VARCHAR(255) NOT NULL,
    hashword VARCHAR(255) NOT NULL,
    delivery_charge INT NOT NULL,
    return_charge INT NOT NULL
);

INSERT INTO clients (name,username,hashword, delivery_charge, return_charge)
SELECT name, username, hashword, delivery_charge, return_charge FROM users
INNER JOIN client_info on users.id = client_info.client_id;

DROP TABLE client_info;

DELETE FROM users where user_role = 'client';

ALTER TABLE users DROP COLUMN user_role;
DROP TYPE UserRole;
ALTER TABLE users RENAME TO admins;