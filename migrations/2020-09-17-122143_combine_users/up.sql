-- Your SQL goes here

-- Convert admins table into users table
ALTER TABLE admins RENAME TO users;
CREATE TYPE UserRole AS ENUM('admin', 'client', 'driver');
ALTER TABLE users ADD COLUMN user_role UserRole;
-- Set existing admins role
UPDATE users SET user_role='admin';
ALTER TABLE users ALTER COLUMN user_role SET NOT NULL;
-- Copy over clients
INSERT INTO users(name,username,hashword,user_role) 
SELECT name,username,hashword, 'client' as user_role FROM clients;
-- Create a separate table to store extra client info
CREATE TABLE client_info (
    client_id INT PRIMARY KEY NOT NULL,
    delivery_charge INT NOT NULL,
    return_charge INT NOT NULL
);
-- Copy over those extra details
INSERT INTO client_info (client_id, delivery_charge, return_charge)
SELECT users.id as client_id, clients.delivery_charge, clients.return_charge FROM users
INNER JOIN clients on users.username = clients.username;
-- Remove the redundant client table
DROP TABLE clients;